{-# OPTIONS_GHC -Wall #-}
module TestB where

g :: Int -> Int
g n | n < 5     = 32
    | otherwise = n * 4

g1 = 23