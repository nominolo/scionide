{-# LANGUAGE OverloadedStrings #-} 

import Language.Haskell.Scion.Types
import Language.Haskell.Scion.Dispatcher
import Language.Haskell.Scion.Packages
import Language.Haskell.Scion.Cabal

import Control.Applicative ( (<$>) )
import Control.Concurrent ( threadDelay )
import Control.Concurrent.MVar
import Control.Exception ( finally )
import Data.Binary
import Data.List ( intersect, sort )
import Data.Monoid ( mempty )
import System.Directory ( doesFileExist, getCurrentDirectory, removeFile,
          removeDirectoryRecursive )
import System.FilePath ( (</>) )
import Data.Time.Clock
import System.IO.Temp ( withTempDirectory )
import qualified Data.Text as T
import qualified Data.Map as M
import Test.Framework
import Test.Framework.Providers.HUnit
import Test.HUnit
import Distribution.InstalledPackageInfo ( installedPackageId )

main = defaultMain tests

tests =
  [ testGroup "Serialisation"
    [ testCase "WorkerCommand" test_serializeWorkerCommand
    , testCase "WorkerResponse" test_serializeWorkerResponse             
    ]
  , testGroup "Worker"
    [ testCase "start/stop" test_workerStartStop
    , testCase "Init GHC API" test_workerGhc
    , testCase "Init GHC API w/ warnings 1" test_workerGhc_withWarnings
    , testCase "Init GHC API w/ warnings 2" test_workerGhc_withWarnings2
    ]
  , testGroup "Commands"
    [ testCase "Parse file options" test_workerGhc_parseFileOptions
    , testCase "Parse file options (unknown option)"
               test_workerGhc_parseFileOptions2
    , testCase "Parse file options (parse error)"
               test_workerGhc_parseFileOptions3
    , testCase "Parse file options (parse error 2)"
               test_workerGhc_parseFileOptions4
    , testCase "Parse file options (unknown flag)"
               test_workerGhc_parseFileOptions5
    , testCase "Parse file options (file does not exist)"
               test_workerGhc_parseFileOptions6
    , testCase "Parse imports" test_workerGhc_parseImports
    , testCase "Parse imports (parse error)"
               test_workerGhc_parseImports2
    , testCase "Parse imports quick" test_workerGhc_parseImportsQuick
    , testCase "getCabalComponents" test_getCabalComponents
    ]
  -- , testGroup "Compile"
  --   [ testCase "single (no warns/errs)" test_compile_simple1
  --   , testCase "single (1 warning)" test_compile_simple2
  --   , testCase "single (1 warning, no code)" test_compile_simple3
  --   , testCase "single (header error)" test_compile_error1
  --   , testCase "single (type error)" test_compile_error2
  --   ]
  , testGroup "Shake"
    [ -- testCase "project1" test_shake_project1
    -- , 
      testCase "dummy1" test1
    , testCase "dummy2" test2
    , testCase "paths" test_paths
--    , testCase "self" test_self
    ]
  ]

testSerialization :: (Eq a, Show a, Binary a) => a -> IO ()
testSerialization thing =
  let bs = encode thing in
  case decodeOrFail bs of
    Left (_, _, err) ->
      assertFailure $ "Could not decode " ++ show thing
    Right (_, _, thing') ->
      thing @=? thing'

test_serializeWorkerCommand = do
  mapM_ testSerialization [ GetWorkerVersion
                          , CompileFile "foo.hs"
                          , StopWorker
                          ]

test_serializeWorkerResponse = do
  mapM_ testSerialization [ WorkerVersion 123
                          , UnknownCommand
                          , WorkerAck
                          , CompilationResult [] [] ]

test_workerStartStop = do
  h <- startWorker
  stopWorker h

test_workerGhc_withWarnings = do
  h <- startWorker
  warns <- setupWorkerGhcApi h ["-smp"]
  stopWorker h
  ["-smp is deprecated: Use -threaded instead"] @=? warns

test_workerGhc_withWarnings2 = do
  h <- startWorker
  warns <- setupWorkerGhcApi h ["-foobarbaz"]
  stopWorker h
  ["Ignoring flags: [\"-foobarbaz\"]"] @=? warns

test_workerGhc = withWorker [] $ \h [] -> do
  ans <- workerCommand h GetInstalledPackages
  let Just (InstalledPackages pkgs) = ans
  let pkgNames = map packageName pkgs
  let expectedPackages = ["base", "ghc", "ghc-prim"]
  --mapM_ (print . installedPackageId) pkgs
  assertBool ("Packages should include " ++ show expectedPackages ++
             "\nGot: " ++ show pkgNames) $
    (expectedPackages `intersect` pkgNames) == expectedPackages

test_workerGhc_parseFileOptions = withWorker [] $ \h [] -> do
  Just ans <- workerCommand h (ParseOptions "tests/data/test1.hs")
  case ans of
    ParsedOptions opts ->
      ["-Wall","-XBangPatterns","-XPatternGuards"] @=? sort opts
    _ -> assertFailure (show ans)

test_workerGhc_parseFileOptions2 = withWorker [] $ \h [] -> do
  Just ans <- workerCommand h (ParseOptions "tests/data/test2.hs")
  case ans of
    WorkerSourceErrors errs ->
      [OtherError (SourceSpan 1 14 1 23) "Unsupported extension: Kerfuffle"]
        @=? errs
    _ -> assertFailure (show ans)

test_workerGhc_parseFileOptions3 = withWorker [] $ \h [] -> do
  Just ans <- workerCommand h (ParseOptions "tests/data/test3.hs")
  case ans of
    WorkerSourceErrors [OtherError _ text] ->
      "Cannot parse LANGUAGE pragma" @=? T.take 28 text
    _ -> assertFailure (show ans)

test_workerGhc_parseFileOptions4 = withWorker [] $ \h [] -> do
  Just ans <- workerCommand h (ParseOptions "tests/data/test4.hs")
  case ans of
    WorkerSourceErrors [OtherError _ text] ->
      "Cannot parse LANGUAGE pragma" @=? T.take 28 text
    _ -> assertFailure (show ans)

test_workerGhc_parseFileOptions5 = withWorker [] $ \h [] -> do
  Just ans <- workerCommand h (ParseOptions "tests/data/test5.hs")
  case ans of
    ParsedOptions opts ->
      -- Flags are verified later
      ["-auto-alibi"] @=? opts
    _ -> assertFailure (show ans)

test_workerGhc_parseFileOptions6 = withWorker [] $ \h [] -> do
  Just ans <- workerCommand h (ParseOptions "thisdoesnotexist/test5.hs")
  case ans of
    WorkerOtherError _ -> return ()
    _ -> assertFailure (show ans)

test_workerGhc_parseImports = withWorker [] $ \h [] -> do
  Just ans <- workerCommand h (ParseImports "tests/data/test1.hs")
  case ans of
    ParsedImports header -> do
      mkModuleName "Main" @=? moduleHeaderModuleName header
      ["-Wall","-XBangPatterns","-XPatternGuards"] @=?
        sort (moduleHeaderOptions header)
      map mkModuleName ["Prelude"] @=?
        map importModuleName (moduleHeaderImports header)
    _ -> assertFailure (show ans)

test_workerGhc_parseImports2 = withWorker [] $ \h [] -> do
  Just ans <- workerCommand h (ParseImports "tests/data/test3.hs")
  case ans of
    WorkerSourceErrors [OtherError _ text] ->
      "Cannot parse LANGUAGE pragma" @=? T.take 28 text
    _ -> assertFailure (show ans)

test_workerGhc_parseImportsQuick = withWorker [] $ \h [] -> do
  parseIt h
  parseIt h
  parseIt h
 where
   parseIt h = do
     startTime <- getCurrentTime
     Just ans <- workerCommand h (ParseImports "tests/data/test1.hs")
     case ans of
       ParsedImports header -> do
         time <- (`diffUTCTime` startTime) <$> getCurrentTime
         putStrLn ("ParseQuick: " ++ show time)
         mkModuleName "Main" @=? moduleHeaderModuleName header
         ["-Wall","-XBangPatterns","-XPatternGuards"] @=?
           sort (moduleHeaderOptions header)
         map mkModuleName ["Prelude"] @=?
           map importModuleName (moduleHeaderImports header)
       _ -> assertFailure (show ans)


------------------------------------------------------------------------

expectFileExists :: FilePath -> IO ()
expectFileExists file = do
  ex <- doesFileExist file
  assertBool ("File " ++ show file ++ " does not exist") ex

expectFileNotExists :: FilePath -> IO ()
expectFileNotExists file = do
  ex <- doesFileExist file
  assertBool ("File " ++ show file ++ " does exist") (not ex)

test_compile_simple1 =
  withTempDirectory "tests" "dist." $ \distDir -> do
  withWorker ["-odir", distDir, "-hidir", distDir] $ \h [] -> do
    Just ans <- workerCommand h (CompileFile "tests/data/test1.hs")
    case ans of
      CompilationResult [] [] -> do
        expectFileExists (distDir </> "Main.hi")
        expectFileExists (distDir </> "Main.o")
      _ -> assertFailure (show ans)

test_compile_simple2 =
  withTempDirectory "tests" "dist." $ \distDir -> do
  withWorker ["-odir", distDir, "-hidir", distDir] $ \h [] -> do
    Just ans <- workerCommand h (CompileFile "tests/data/test6.hs")
    case ans of
      CompilationResult [warn] [] -> do
        OtherWarning (SourceSpan 5 1 5 28) "Top-level binding with no type signature: main :: IO ()" @=? warn
        expectFileExists (distDir </> "Main.hi")
        expectFileExists (distDir </> "Main.o")
      _ -> assertFailure (show ans)

test_compile_simple3 = do
  withTempDirectory "tests" "dist." $ \distDir -> do
  withWorker ["-odir", distDir, "-hidir", distDir, "-O0"] $ \h [] -> do
    Just ans <- workerCommand h (CompileFile "tests/data/test6.hs")
    case ans of
      CompilationResult [warn] [] -> do
        OtherWarning (SourceSpan 5 1 5 28) "Top-level binding with no type signature: main :: IO ()" @=? warn
        expectFileExists (distDir </> "Main.hi")
        expectFileExists (distDir </> "Main.o")
      _ -> assertFailure (show ans)

test_compile_error1 =
  withTempDirectory "tests" "dist." $ \distDir -> do
  withWorker ["-odir", distDir, "-hidir", distDir] $ \h [] -> do
    Just ans <- workerCommand h (CompileFile "tests/data/test2.hs")
    case ans of
      CompilationResult [] [_err] -> do
        expectFileNotExists (distDir </> "Main.hi")
        expectFileNotExists (distDir </> "Main.o")
      _ -> assertFailure (show ans)

test_compile_error2 =
  withTempDirectory "tests" "dist." $ \distDir -> do
  withWorker ["-odir", distDir, "-hidir", distDir] $ \h [] -> do
    Just ans <- workerCommand h (CompileFile "tests/data/test7.hs")
    case ans of
      CompilationResult [] [OtherError _ msg] -> do
        "Couldn't match expected type `Int ->" @=? T.take 36 msg
        expectFileNotExists (distDir </> "Main.hi")
        expectFileNotExists (distDir </> "Main.o")
      _ -> assertFailure (show ans)

test_getCabalComponents = do
  comps <- getCabalComponents "tests/data/project1/project1.cabal"
  [CLib "project1",CExe "test1"] @=? comps

test_shake_project1 = do
  --buildCabalProject "tests/data/project1/project1.cabal"
  cfg <- mkCabalConfig "tests/data/project1/project1.cabal" ".scion"
  workers <- newWorkers
  msg1 <- getComponentNotes cfg workers (CLib "project1")
  print msg1
  stopWorkers workers

test1 :: IO ()
test1 = do
  cfg <- mkCabalConfig "tests/data/project1/project1.cabal" ".scion"
  workers <- newWorkers
  (do
    warns1 <- show <$> getComponentNotes cfg workers (CLib "project1")
    sleep 1
    warns2 <- show <$> getComponentNotes cfg workers (CLib "project1")
    warns1 @=? warns2
    sleep 1
    removeFile "tests/data/project1/.scion/setup-config"
    warns3 <- show <$> getComponentNotes cfg workers (CLib "project1")
    putStrLn warns1
    putStrLn warns3
    warns1 @=? warns3
   `finally` (do
     removeDirectoryRecursive "tests/data/project1/.scion"
     stopWorkers workers))

test_paths :: IO ()
test_paths = do
  cfg <- mkCabalConfig "tests/data/project2/project2.cabal" ".scion"
  workers <- newWorkers
  (do
    warns <- getComponentNotes cfg workers (CExe "project2")
    print warns
    case M.lookup "test1.hs" warns of
      Just (_,_,e) -> e @=? []
      _ -> assertFailure "No messages for test1.hs"
    return ()
   `finally` (do
     removeDirectoryRecursive "tests/data/project2/.scion"
     stopWorkers workers))

test2 :: IO ()
test2 = do
  cfg <- mkCabalConfig "tests/data/project1/project1.cabal" ".scion"
  workers <- newWorkers
  print =<< getComponentNotes cfg workers (CExe "test1")
  stopWorkers workers

{-
test_self :: IO ()
test_self = do
  cfg <- mkCabalConfig "library/scionide.cabal" ".scion2"
  workers <- newWorkers
  (do
    warns1 <- show <$> getComponentNotes cfg workers (CLib "scionide")
    sleep 1
    appendFile "library/Language/Haskell/Scion/Session.hs" " "
    warns2 <- show <$> getComponentNotes cfg workers (CLib "scionide")
    warns1 @=? warns2
    -- sleep 1
    -- removeFile "tests/data/project1/.scion/setup-config"
    -- warns3 <- show <$> getComponentNotes cfg workers (CLib "project1")
    -- putStrLn warns1
    -- putStrLn warns3
    -- warns1 @=? warns3
   `finally` (do
     removeDirectoryRecursive "library/.scion2"
     stopWorkers workers))
-}

sleep :: Int -> IO ()
sleep n = threadDelay (n * 1000000)
