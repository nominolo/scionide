{-# LANGUAGE StandaloneDeriving, DeriveFunctor, ScopedTypeVariables #-}
{-# LANGUAGE BangPatterns, OverloadedStrings #-}
module Main where

import Language.Haskell.Scion.Version as Scion
import Language.Haskell.Scion.Types as Scion
import Language.Haskell.Scion.Utils.IO

import Control.Applicative
import Control.Concurrent ( threadDelay )
import Control.Exception
import Data.Binary ( Binary )
import Data.IORef
import Data.Monoid
import qualified Data.Text as T
import GHC hiding ( mkModuleName, ModuleName, SourceError, Warning )
import qualified GHC as Ghc
import GHC.Paths ( libdir )
import Outputable ( renderWithStyle, showPpr, Outputable, sdocWithDynFlags, mkErrStyle, withPprStyle, showSDoc )
import MonadUtils ( liftIO )
import Exception ( ghandle )
import DynFlags ( defaultLogAction, supportedLanguagesAndExtensions )
import Bag ( bagToList )
import HscTypes ( srcErrorMessages )
import ErrUtils ( ErrMsg(..) )
import DriverPipeline ( oneShot )
import DriverPhases ( Phase(..) )
import Distribution.Package ( InstalledPackageId(..) )
import Packages
import HeaderInfo ( getOptionsFromFile, getOptions, getImports )
import StringBuffer ( hGetStringBuffer )
import System.Directory
import System.Exit
import System.FilePath
import System.IO

main :: IO ()
main = workerMain

workerVersion :: Int
workerVersion = 1

message :: String -> IO ()
message text = hPutStrLn stderr text

-- | The part of the worker state that can only be set once.
data StaticWorkerState = StaticWorkerState
  { workerFlags :: [T.Text]
  , workerDirectory :: FilePath
  , workerReplyHandle :: Handle
  }

workerMain :: IO ()
workerMain = do
  replyHandle <- makeExclusive stdout stderr
  ensureBinaryMode stdin
  ensureBinaryMode replyHandle
  mb_static_state <- protoWorkerLoop replyHandle
  case mb_static_state of
    Nothing -> return ()
    Just static_state -> mainWorkerLoop static_state

-- | This is the event loop of the worker before it has been decided
-- how to boot up the API.  It responds only to basic commands such as
-- the worker version.
--
protoWorkerLoop :: Handle -> IO (Maybe StaticWorkerState)
protoWorkerLoop replyHandle = do
  mb_msg <- recvMessageFromHandle stdin
  case mb_msg of 
    Nothing -> exitWith (ExitFailure 1)
    Just msg ->
      case msg of
        GetWorkerVersion -> do
          sendMessageToHandle replyHandle (WorkerVersion Scion.scionVersion)
          protoWorkerLoop replyHandle
        StopWorker -> do
          sendMessageToHandle replyHandle WorkerAck
          return Nothing
        InitGhcWorker flags -> do
          workingDir <- getCurrentDirectory
          return (Just (StaticWorkerState{ workerReplyHandle = replyHandle
                                         , workerDirectory = workingDir
                                         , workerFlags = flags }))
        _ -> do
          sendMessageToHandle replyHandle UnknownCommand
          protoWorkerLoop replyHandle
  
mainWorkerLoop :: StaticWorkerState -> IO ()
mainWorkerLoop state = do
  setCurrentDirectory (workerDirectory state)
  ghcApiMainLoop (map T.unpack (workerFlags state)) (workerReplyHandle state)

ghcApiMainLoop :: [String] -> Handle -> IO ()
ghcApiMainLoop args0 replyHandle = do
  messages <- newIORef []
  let logger dflags severity span pprStyle message = do
        hPutStrLn stderr (renderWithStyle dflags message pprStyle)
  handleWorkerError replyHandle $ do
  let args1 = map (mkGeneralLocated "worker flags") args0
  (args2, staticFlagWarnings) <- parseStaticFlags args1
  runGhc (Just libdir) $ do
    dflags0 <- GHC.getSessionDynFlags
    let dflags1 = dflags0{ ghcMode = OneShot
                         , ghcLink = LinkBinary
                         , hscOutName = error "hscOutName not set"
                         , verbosity = 1
                         , log_action = logger
                         }
    (dflags2, fileishArgs, dynFlagWarnings)
      <- GHC.parseDynamicFlags dflags1 args2
    let extraArgWarnings
          | null fileishArgs = []
          | otherwise =
            [T.pack $ "Ignoring flags: " ++ show (map unLoc fileishArgs)]
    let allWarnings =
          map (T.pack . unLoc) (staticFlagWarnings ++ dynFlagWarnings) ++
              extraArgWarnings

    GHC.defaultCleanupHandler dflags2 $ do
      _ <- GHC.setSessionDynFlags dflags2
      dflags3 <- GHC.getSessionDynFlags
      _hsc_env <- GHC.getSession
      liftIO $ sendMessageToHandle replyHandle $ GhcWorkerReady allWarnings
      ghcApiMainLoop' replyHandle

handleWorkerError :: Handle -> IO () -> IO ()
handleWorkerError replyHandle body = do
  ghandle (\exception -> do
    hFlush stderr
    hFlush stdout
    case fromException exception of
      Just (ioerr :: IOException) ->
        fatalMessage ("IOExc:" ++ (show ioerr))
      _ ->
        case fromException exception of
          Just UserInterrupt ->
            exitWith (ExitFailure 1)
          Just StackOverflow ->
            fatalMessage "stack overflow: use +RTS -K<size> to increase it"
          _ ->
            case fromException exception of
              Just (ex :: ExitCode) ->
                fatalMessage "Worker called exit()"
              _ ->
                fatalMessage $ "GHC Panic: " ++ show exception
     ) (-- error messages propagated as exceptions
        ghandle (\(ge :: GhcException)-> do
           hFlush stdout
  	   case ge of
             PhaseFailed _ code -> exitWith code
             Signal _ -> exitWith (ExitFailure 1)
             _ -> fatalMessage (show ge)
	  ) body)
 where
   fatalMessage :: String -> IO ()
   fatalMessage str = do
     hPutStrLn stderr ("FATAL: " ++ str)
     hFlush stderr
     sendMessageToHandle replyHandle (WorkerFailure (T.pack str))
     -- give parent a chance to receive our goodbye message
     threadDelay 1000000
     hClose stderr
     hClose replyHandle
     exitWith (ExitFailure 1)

ghcApiMainLoop' :: Handle -> Ghc ()
ghcApiMainLoop' replyHandle = do
  mb_msg <- liftIO $ recvMessageFromHandle stdin
  case mb_msg of
    Nothing -> liftIO (exitWith (ExitFailure 1))
    Just msg ->
      case msg of
        StopWorker -> do
          reply WorkerAck
          return ()
        other -> do
          handleStandardErrors $ do
            handleWorkerCommand other >>= reply
          ghcApiMainLoop' replyHandle
 where
   reply :: Binary a => a -> Ghc ()
   reply v = liftIO $ sendMessageToHandle replyHandle v

   handleStandardErrors act = do
     dflags <- getSessionDynFlags
     ghandle (\(err :: IOException) -> do
                  reply (WorkerOtherError (T.pack (show err)))) $   
       handleSourceError (\err -> do
           let errs = map (errorMessageToSourceError dflags) $
                        bagToList (srcErrorMessages err)
           reply (WorkerSourceErrors errs))
         act

handleWorkerCommand :: WorkerCommand -> Ghc WorkerResponse
handleWorkerCommand command =
  case command of
    GetWorkerVersion ->
      return $! WorkerVersion Scion.scionVersion
    GetInstalledPackages ->
      InstalledPackages <$> getInstalledPackages
    GetSupportedLanguages ->
      SupportedLanguages <$> getSupportedLanguages
    ParseOptions file ->
      ParsedOptions <$> parseFileOptions file
    ParseImports file ->
      ParsedImports <$> parseImports file
    CompileFile file -> do
      (warns, errs) <- compileFile file
      return $! CompilationResult warns errs
    _ ->
      return UnknownCommand

deriving instance Functor InstalledPackageInfo_

getSupportedLanguages :: Ghc [T.Text]
getSupportedLanguages = do
  return $ map T.pack supportedLanguagesAndExtensions

getInstalledPackages :: Ghc [Scion.InstalledPackage]
getInstalledPackages = do
  dflags <- getSessionDynFlags
  let Just pkg_configs = pkgDatabase dflags
  return (map (translateModuleNames dflags) pkg_configs)
 where
   translateModuleNames dflags pkgConf = fmap (translateModuleName dflags) pkgConf
   translateModuleName dflags modName = ModuleName (T.pack (showPpr dflags modName))

printLoadedPackages :: Ghc ()
printLoadedPackages = do
  dflags <- getSessionDynFlags
  let Just pkg_configs = pkgDatabase dflags
  liftIO (mapM_ (printPkgId dflags) pkg_configs)
 where
   printPkgId dflags pkg_conf = do
     let InstalledPackageId pkg_id = installedPackageId pkg_conf
     putStrLn $ pkg_id ++ (if not (exposed pkg_conf) then " [hidden]" else "")
     putStrLn $ "  " ++ showPpr dflags (exposedModules pkg_conf)

parseFileOptions :: FilePath -> Ghc [T.Text]
parseFileOptions file = do
  dflags <- getSessionDynFlags
  liftIO $
    map (T.pack . unLoc) <$> getOptionsFromFile dflags file

parseImports :: FilePath -> Ghc ModuleHeader
parseImports file = do
  dflags <- getSessionDynFlags
  liftIO $ do
    -- HeaderInfo.getOptions is pure but may throw an exception when
    -- the result is forced (WTF?!).  This reads the file twice, but
    -- I'll take correctness over performance anytime.
    options <- map (T.pack . unLoc) <$> getOptionsFromFile dflags file
    buf <- hGetStringBuffer file
    (source_imports, normal_imports, module_name0)
      <- getImports dflags buf file file
    let imports = map (mkImport True) source_imports ++
                  map (mkImport False) normal_imports
    return $! ModuleHeader
      { moduleHeaderModuleName = ghcToModuleName (unLoc module_name0)
      , moduleHeaderOptions = options
      , moduleHeaderImports = imports
      }
 where
   mkImport isSource imp0 =
     let imp = unLoc imp0 in
     ImportDependency
       { importModuleName = ghcToModuleName (unLoc (ideclName imp))
       , importPackageName = Nothing
       , importSource = isSource
       , importSafe = ideclSafe imp
       }

ghcToModuleName :: Ghc.ModuleName -> ModuleName
ghcToModuleName mn = mkModuleName (T.pack (moduleNameString mn))

ghcToSrcSpan :: Ghc.SrcSpan -> SourceSpan
ghcToSrcSpan (UnhelpfulSpan _) = mempty
ghcToSrcSpan (RealSrcSpan rspan) =
  SourceSpan (srcSpanStartLine rspan)
             (srcSpanStartCol rspan)
             (srcSpanEndLine rspan)
             (srcSpanEndCol rspan)

errorMessageToSourceError :: DynFlags -> ErrMsg -> SourceError
errorMessageToSourceError dflags0 msg =
  -- TODO: Parse message
  let sdoc =
        sdocWithDynFlags $ \dflags ->
          let style = mkErrStyle dflags (errMsgContext msg)
          in withPprStyle style (errMsgShortDoc msg)
      s : _ = errMsgSpans msg
      sp = ghcToSrcSpan s
  in OtherError sp (T.pack (showSDoc dflags0 sdoc))

compileFile :: FilePath -> Ghc ([Warning], [SourceError])
compileFile path = do
  dflags0 <- getSessionDynFlags
  warnRef <- liftIO $ newIORef []

  let logAction dflags severity loc style doc =
        case severity of
          SevWarning -> do
            let msg = renderWithStyle dflags doc style
                span' = ghcToSrcSpan loc
            let warn = OtherWarning span' (T.pack msg)
            atomicModifyIORef warnRef $ \warns -> (warn : warns, ())
          _ -> defaultLogAction dflags severity loc style doc

  let !dflags1 = dflags0{ ghcMode = OneShot
                        , ghcLink = NoLink
                        , hscOutName = error "hscOutName not set"
                        , verbosity = 1
                        , log_action = logAction
                        }
  GHC.setSessionDynFlags dflags1
  hsc_env <- GHC.getSession

  handleSourceError (\err -> do
      let errs = map (errorMessageToSourceError dflags1) $
                   bagToList (srcErrorMessages err)
      warns <- liftIO $ readIORef warnRef
      return $! (reverse warns, errs)) $ do

    liftIO (oneShot hsc_env StopLn [(path, Nothing)])

    warns <- liftIO $ readIORef warnRef
    return (reverse warns, [])

-- instance Show GHC.ModuleName where
--   show mn = showPpr mn
