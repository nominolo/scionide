.PHONY: default
default: test

VERSION = 0.1.2



-include build.mk

PWD := `pwd`
DIST ?= dist
HC ?= ghc
HC_PKG ?= ghc-pkg
CABAL ?= cabal
PREFIX ?= $(HOME)/local/bin

DIST_LIB := $(abspath $(DIST)/scionide)
DIST_LIB_a := $(DIST_LIB)/build/libHSscionide-$(shell sed -n -f scripts/get-version.sed library/scionide.cabal).a
HSSRCS := $(shell find library -name '*.hs')

DIST_WORKER := $(abspath $(DIST)/scion-worker)
WORKER_EXE := $(DIST_WORKER)/build/scion-worker/scion-worker
WORKER_SRCS := $(shell find worker -name '*.hs')

.PHONY: config build worker worker-config clean
config: $(DIST_LIB)/setup-config
build: $(DIST_LIB_a)
worker-config: $(DIST_WORKER)/setup-config
worker: $(WORKER_EXE)
all: $(DIST_LIB_a) $(WORKER_EXE)
clean:
	rm -rf $(DIST)

$(DIST_LIB)/setup-config: library/scionide.cabal
	@mkdir -p $(DIST_LIB)
	cd library && $(CABAL) configure --builddir=$(DIST_LIB) \
		--with-compiler=$(HC) --with-hc-pkg=$(HC_PKG)

$(DIST_LIB_a): $(DIST_LIB)/setup-config $(HSSRCS)
	cd library && $(CABAL) build --builddir=$(DIST_LIB)

$(DIST_WORKER)/setup-config: worker/scion-worker.cabal $(DIST_LIB_a)
	@mkdir -p $(DIST_WORKER)
	cd worker && $(CABAL) configure --builddir=$(DIST_WORKER) \
		--with-compiler=$(HC) --with-hc-pkg=$(HC_PKG) \
		--package-db=$(DIST_LIB)/package.conf.inplace

$(WORKER_EXE): $(DIST_WORKER)/setup-config $(WORKER_SRCS)
	cd worker && $(CABAL) build --builddir=$(DIST_WORKER)

.PHONY: test
test: $(DIST)/LibraryTests
	env SCION_WORKER=$(WORKER_EXE) ./$<

$(DIST)/LibraryTests: tests/LibraryTests.hs $(WORKER_EXE) $(DIST_LIB_a)
	$(HC) --make -threaded -package-db=$(DIST_LIB)/package.conf.inplace \
		-package scionide -hide-package binary \
		-o $@ $<

install-deps:
	cd library && $(CABAL) install --enable-documentation --enable-library-profiling --haddock-hoogle --haddock-html --only-dependencies
	cd worker && $(CABAL) install --enable-documentation --enable-library-profiling --haddock-hoogle --haddock-html --only-dependencies

# install-deps:
# 	time cabal install -j --enable-documentation --enable-library-profiling --haddock-hoogle --haddock-html --only-dependencies
