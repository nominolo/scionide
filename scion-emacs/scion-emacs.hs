{-# LANGUAGE BangPatterns #-}
{-# LANGUAGE PatternGuards #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE ScopedTypeVariables #-}
module Main ( main ) where

import Control.Applicative
import Control.Concurrent.MVar
import Control.Exception ( catches, Handler(..) )
import Control.Monad ( guard )
import Data.IORef
import Data.Monoid
import Language.Haskell.Scion.Types
import Language.Haskell.Scion.Dispatcher
import Language.Haskell.Scion.Cabal
import Language.Haskell.Scion.Session
import Data.Time.Clock
import Network ( listenOn, PortID(..) )
import Numeric ( showHex )
import System.IO (stdin, stdout, hSetBuffering, hFlush, BufferMode(..), hPutStrLn, stderr)
import System.FilePath ( (</>) )
import Network.Socket hiding (send, sendTo, recv, recvFrom)
import Network.Socket.ByteString
import qualified Control.Exception as E
import Data.AttoLisp ( ToLisp(..) )
import qualified Data.AttoLisp as Lisp
--import qualified Data.Attoparsec.ByteString as Atto
import qualified Data.Attoparsec.ByteString.Lazy as Atto.Lazy
import Data.Aeson hiding ( Error )
import qualified Data.Aeson as Json
import qualified Data.Aeson.Types as Json
import qualified Data.Map as Map
import qualified Data.HashMap.Strict as HM
import qualified Data.Text as Text
import qualified Data.Vector as Vector
import qualified Data.ByteString.Lazy.Char8 as LC8
import qualified Data.ByteString.Char8 as BC8

main :: IO ()
main = mainLoop

logInfo :: String -> IO ()
logInfo msg = hPutStrLn stderr msg

listenOnOneOf :: [PortID] -> IO Socket
listenOnOneOf (p:ps) =
  listenOn p `E.catch` 
    (\(ex :: IOError) ->
       if null ps then E.throwIO ex else listenOnOneOf ps)
listenOnOneOf [] = error "listenOnOneOf: no port given"

mainLoop :: IO ()
mainLoop = withSocketsDo $ do
  sock <- listenOnOneOf (map PortNumber [4005..4040])
  realNo <- socketPort sock
  putStrLn $ "=== Listening on port: " ++ show realNo
  hFlush stdout
  let run True = return ()
      run _ = 
       E.handle (\(e::E.IOException) -> do
                    logInfo ("caught :" ++ (show e) ++
                             "\n\nwaiting for next client")
                    run False) $ do
         (sock', _addr) <- accept sock
         sock_conn <- mkSocketConnection sock'
         sessions <- newIORef noSessions
         workers <- newMVar =<< initWorkers
         stop_server <- handleClient sessions workers sock_conn
         run stop_server
  run False

data SocketConnection = SockConn Socket (IORef BC8.ByteString)

mkSocketConnection :: Socket -> IO SocketConnection
mkSocketConnection sock = 
    do r <- newIORef BC8.empty; return $ SockConn sock r

data Sessions = Sessions
  { nextSessionId :: !Int
  , allSessions   :: !(Map.Map Int Session)
  }

noSessions :: Sessions
noSessions = Sessions 1 Map.empty

handleClient :: IORef Sessions -> MVar Workers -> SocketConnection -> IO Bool
handleClient sessionsRef workers conn@(SockConn _sock _) = do
  bs <- getLineSock conn
  if LC8.length bs == 0 then
    return True
   else do
    let line = LC8.drop 6 bs
    case Atto.Lazy.parse Lisp.lisp line of      
      Atto.Lazy.Fail _ _ msg -> do
        hPutStrLn stderr $ "Message parse error: " ++ msg
        return True
      Atto.Lazy.Done _ exp
       | Lisp.List [key, cmd, _package, session, counter] <- exp
       , key == emacsRex
       -> do
        startTime <- getCurrentTime
        ans <- handleCommand sessionsRef workers cmd
        totalTime <- (`diffUTCTime` startTime) <$> getCurrentTime
        infoLog $ "Command time: " ++ show totalTime
          --dispatchCommand sessionsRef cmd
        reply conn $ Lisp.List [sym ":return", toLisp ans, counter]
        handleClient sessionsRef workers conn

reply :: SocketConnection -> Lisp.Lisp -> IO ()
reply conn expr = do
  let !resp = Lisp.encode expr <> LC8.singleton '\n'
  let !hdr = mkHeader (fromIntegral (LC8.length resp))
  let msg = hdr <> resp
  putSock conn msg


mkHeader :: Int -> LC8.ByteString
mkHeader len =
  LC8.pack $ reverse . take 6 $ reverse (showHex len "") ++ repeat '0'

emacsRex :: Lisp.Lisp
emacsRex = sym ":emacs-rex"

sym :: Text.Text -> Lisp.Lisp
sym = Lisp.Symbol

infoLog :: String -> IO ()
infoLog msg = hPutStrLn stderr msg

data OkErr a = Ok !a | Error !Text.Text
  deriving (Eq, Ord, Show)

instance Functor OkErr where
  fmap f (Ok x)      = Ok (f x)
  fmap f (Error msg) = Error msg

instance ToLisp a => ToLisp (OkErr a) where
  toLisp (Ok x) = Lisp.List [sym ":ok", toLisp x]
  toLisp (Error msg) = Lisp.List [sym ":error", toLisp msg]

data Command = Command Text.Text
                       (IORef Sessions -> MVar Workers -> Json.Value
                        -> IO (OkErr Json.Value))

parseIO :: Value -> (Value -> Json.Parser a) -> IO a
parseIO val parser = do
  let Right x = Json.parseEither parser val
  return x

handleCommand :: IORef Sessions -> MVar Workers -> Lisp.Lisp
              -> IO (OkErr Lisp.Lisp)
handleCommand sessions workers (Lisp.List (Lisp.Symbol cmdName : args)) =
  case HM.lookup cmdName commandMap of
    Nothing ->
      return $ Error $ "Unknown command: " <> cmdName
    Just (Command _ handler) -> do
      case Json.parseMaybe fromLisp (Lisp.List args) of
        Nothing ->
          return $ Error "Could not parse arguments"
        Just args' -> do
          fmap toLisp <$> handler sessions workers args'
           `catches`
             [ Handler $ \(e :: CabalError) ->
                 return (Error $ Text.pack (show e))
             , Handler $ \(e :: ScionError) ->
                 return (Error $ Text.pack (show e))
             ]
handleCommand _sessions _ _ = 
  return $ Error $ "Could not parse command"

commandMap :: HM.HashMap Text.Text Command
commandMap = HM.fromList $
  [ (name, cmd) | cmd@(Command name _) <- commands ]

commands :: [Command]
commands =
  [ Command "connection-info" $ \_ _ _ ->
      return (Ok (Json.object ["pid" .= (1234 :: Int),
                               "version" .= (2 :: Int)]))

  , Command "list-supported-languages" $ \_ _ _ -> do
      withWorker [] $ \h _ -> do
        ans <- workerCommand h GetSupportedLanguages
        case ans of
          Just (SupportedLanguages langs) ->
            return $ Ok $! Json.toJSON langs
          _ ->
            return (Error "Worker failed")

   , Command "list-cabal-components" $ \_ _ args -> do
       path <- parseIO args $ withObject "" $ \obj ->
                 obj .: "cabal-file"
       Ok . Json.toJSON <$> getCabalComponents (Text.unpack path)

   , Command "create-session" $ \sessionsRef workers args -> do
       (path, component) <- parseIO args $ withObject "" $ \obj ->
          (,) <$> (obj .: "cabal-file")
              <*> (obj .: "component")
       session <- makeCabalSession component (Text.unpack path)
       notes <- sessionNotes session workers
       atomicModifyIORef sessionsRef $ \(Sessions next sessions) ->
         (Sessions (1 + next) (Map.insert next session sessions), ())
       let jsonNotes = notesToJSON (sessionRootDir session) notes
       return (Ok (toJSON (1 :: Int, jsonNotes, sessionRootDir session)))

   , Command "file-modified" $ \sessionsRef workers args -> do
       path0 <- parseIO args $ withObject "" $ \obj ->
                 obj .: "file"
       let path = Text.unpack path0
       sessions <- readIORef sessionsRef
       let go [] = return (Error "File not found in active sessions")
           go ((sid,s):ss) = do
             mb_msgs <- sessionNotesAfterChange s workers path
             case mb_msgs of
               Nothing -> go ss
               Just notes -> do
                 let jsonNotes = notesToJSON (sessionRootDir s) notes
                 return (Ok (toJSON (jsonNotes, sessionRootDir s)))
       go (Map.toList (allSessions sessions))
   ]

notesToJSON :: FilePath
            -> Messages
            -> Json.Value 
notesToJSON root notes = Json.toJSON
  [ object ["file" .= toJSON (Text.pack (root </> path)),
            "module" .= toJSON (asModuleName header),
            "notes" .= mkNotes warns errs]
  | (path, (header, warns, errs)) <- Map.toList notes ]
 where
   mkNotes warns errs = toJSON $
     map toJSON warns ++ map toJSON errs
   asModuleName (HsFileMeta hdr) =
     moduleName (moduleHeaderModuleName hdr)
   asModuleName CabalMeta = "[Cabal]"

instance ToLisp Json.Value where
  toLisp (Json.Object obj) =
    Lisp.List [ item | (key, value) <- HM.toList obj
                     , item <- [sym (Text.cons ':' key), toLisp value] ]
  toLisp (Json.Array arr) =
    Lisp.List (map toLisp (Vector.toList arr))
  toLisp (Json.String str) =
    Lisp.String str
  toLisp (Json.Number no) =
    Lisp.Number no
  toLisp (Json.Bool b) =
    if b then sym "t" else sym "nil"
  toLisp (Json.Null) = sym "nil"
    
instance ToLisp CabalComponent where
  toLisp (CLib name) =
    Lisp.List [sym ":library", toLisp name]
  toLisp (CExe name) =
    Lisp.List [sym ":executable", toLisp name]
  toLisp (CTest name) =
    Lisp.List [sym ":testsuite", toLisp name]
  toLisp (CBench name) =
    Lisp.List [sym ":benchmark", toLisp name]

newline :: BC8.ByteString
newline = BC8.pack "\n"

getLineSock :: SocketConnection -> IO LC8.ByteString
getLineSock (SockConn sock r) = do 
  buf <- readIORef r
  (line_chunks, buf') <- go buf
  writeIORef r buf'
  return (LC8.fromChunks line_chunks)
 where
    go buf | BC8.null buf = do
      chunk <- recv sock 1024
      if BC8.null chunk
       then return ([], BC8.empty)
       else go chunk
    go buf =
        let (before, rest) = BC8.breakSubstring newline buf in
        case () of
         _ | BC8.null rest -> do
             -- no newline found
             (cs', buf') <- go rest
             return (before:cs', buf')
         _ | otherwise ->
             return ([before], BC8.drop (BC8.length newline) rest)

putSock :: SocketConnection -> LC8.ByteString -> IO ()
putSock (SockConn sock _) lstr = go (LC8.toChunks lstr)
  -- is there a better excption which should be thrown instead?  (TODO)
  -- throw $ mkIOError ResourceBusy ("put in " ++ __FILE__) Nothing Nothing
 where
   go [] = return ()
   go (str:strs) = do
     let l = BC8.length str
     sent <- send sock str
     if (sent /= l) then do
       hPutStrLn stderr $ show l ++ " bytes to be sent but " ++
                        "could only sent : " ++ show sent
      else go strs

------------------------------------------------------------------------

fromLisp :: Lisp.Lisp -> Json.Parser Json.Value
fromLisp (Lisp.List [])    = pure Json.emptyObject
fromLisp (Lisp.List plist) = Json.object <$> pairs plist
fromLisp _ = fail "Top-level object must be a PList"

pairs :: [Lisp.Lisp] -> Json.Parser [(Text.Text, Json.Value)]
pairs plist = go [] plist
 where
   go acc [] = return acc
   go acc (Lisp.Symbol !kw : value : rest) = do
     guard (not (Text.null kw))
     -- Strip leading ":" of Lisp keywords
     let key | Text.head kw == ':' = Text.tail kw
             | otherwise           = kw
     val <- fromLisp' value
     let !acc' = (key, val) : acc
     go acc' rest
   go _ _ = fail "PList expected"

fromLisp' :: Lisp.Lisp -> Json.Parser Json.Value
fromLisp' (Lisp.List l) =
  (Json.object <$> pairs l)
    <|>
  (Json.Array . Vector.fromList <$> mapM fromLisp' l)
fromLisp' (Lisp.String t) = return (Json.String t)
fromLisp' (Lisp.Number n) = return (Json.Number n)
fromLisp' (Lisp.Symbol s)
  | s == "t"      = return (Json.Bool True)
  | s == ":false" = return (Json.Bool False)
  | s == ":null"  = return Json.Null
  | otherwise     = return (Json.String s)
fromLisp' (Lisp.DotList _ _) = fail "Dot lists not supported"

{-
-- TODO: No support for arrays yet
test1 = do
  let Right l = Atto.parseOnly Lisp.lisp inp
  case Json.parseMaybe fromLisp l of
    Nothing -> error "fromLisp failed"
    Just v -> do
      print v
      LC8.putStrLn (Json.encode v)
 where
   inp = "(:command test :args (:file \"test.hs\"))"
-}
