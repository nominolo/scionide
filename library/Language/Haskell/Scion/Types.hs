{-# LANGUAGE DeriveDataTypeable, ExistentialQuantification,
    StandaloneDeriving, DeriveGeneric, OverloadedStrings
 #-}
{-# OPTIONS -fno-warn-orphans #-}
module Language.Haskell.Scion.Types where

import Control.Applicative
import Control.Monad ( when, guard )
import Control.Exception ( Exception(..), SomeException )
import Data.Aeson
import Data.Binary
import Data.Monoid ( Monoid(..) )
import Data.Typeable ( Typeable, cast )
import Data.Version ( Version(..) )
import Data.Word
import Distribution.InstalledPackageInfo (InstalledPackageInfo_(..))
import Distribution.License ( License(..) )
import Distribution.Package ( PackageIdentifier(..), PackageName(..),
                              InstalledPackageId(..) )
import GHC.Generics ( Generic )
import qualified Data.Text as T
import qualified Data.Text.Encoding as T
import qualified Data.Vector as Vec

data WorkerCommand
  = GetWorkerVersion
  | StopWorker
  | InitGhcWorker { ghcFlags :: [T.Text] }
  | GetInstalledPackages
  | GetSupportedLanguages
  | ParseOptions !FilePath
  | ParseImports !FilePath
  | CompileFile !FilePath
  deriving (Eq, Show, Generic)

data WorkerResponse
  = WorkerVersion ![Int]
  | UnknownCommand
  | WorkerAck
  | WorkerFailure !T.Text
  | GhcWorkerReady { warnings :: [T.Text] }
  | CompilationResult [Warning] [SourceError]
  | InstalledPackages [InstalledPackage]
  | SupportedLanguages [T.Text]
  | ParsedOptions [T.Text]
  | ParsedImports !ModuleHeader
  | WorkerSourceErrors [SourceError]
  | WorkerOtherError !T.Text
  deriving (Eq, Show, Generic)

instance Binary WorkerCommand
instance Binary WorkerResponse

instance Binary T.Text where
  put = put . T.encodeUtf8
  get = T.decodeUtf8 <$> get

------------------------------------------------------------------------
data ScionException = forall e. Exception e => ScionException e
  deriving Typeable

instance Show ScionException where
  show (ScionException e) = show e

instance Exception ScionException

scionExceptionToException :: Exception e => e -> SomeException
scionExceptionToException = toException . ScionException

scionExceptionFromException :: Exception e => SomeException -> Maybe e
scionExceptionFromException exc = do
  ScionException e <- fromException exc
  cast e

------------------------------------------------------------------------
data CabalComponent
  = CLib !T.Text
  | CExe !T.Text
  | CTest !T.Text
  | CBench !T.Text
  deriving (Eq, Ord, Show, Generic)

instance Binary CabalComponent

instance ToJSON CabalComponent where
  toJSON (CLib name) = object ["library" .= toJSON name]
  toJSON (CExe name) = object ["executable" .= toJSON name]
  toJSON (CTest name) = object ["testsuite" .= toJSON name]
  toJSON (CBench name) = object ["benchmark" .= toJSON name]

instance FromJSON CabalComponent where
  parseJSON = withObject "CabalComponent" $ \obj ->
               CLib <$> (obj .: "library") <|>
               CExe <$> (obj .: "executable") <|>
               CTest <$> (obj .: "testsuite") <|>
               CBench <$> (obj .: "benchmark")

------------------------------------------------------------------------
data SourceSpan = SourceSpan !Int !Int !Int !Int
  deriving (Eq, Ord, Show, Generic)

instance Binary SourceSpan

instance Monoid SourceSpan where
  mempty = SourceSpan 0 0 (-1) (-1)
  mappend (SourceSpan l1 c1 l2 c2) (SourceSpan l3 c3 l4 c4) =
    SourceSpan lmin cmin lmax cmax
   where (lmin, cmin) = min (l1, c1) (l3, c3)
         (lmax, cmax) = max (l2, c2) (l4, c4)

instance ToJSON SourceSpan where
  toJSON (SourceSpan l1 c1 l2 c2) =
    Array (Vec.fromList (map toJSON [l1, c1, l2, c2]))

instance FromJSON SourceSpan where
  parseJSON = withArray "SourceSpan" $ \elems -> do
    guard (Vec.length elems == 4)
    SourceSpan <$> parseJSON (elems Vec.! 0)
               <*> parseJSON (elems Vec.! 1)
               <*> parseJSON (elems Vec.! 2)
               <*> parseJSON (elems Vec.! 3)

------------------------------------------------------------------------
data CabalError = CabalError String
  deriving (Eq, Ord, Show, Generic, Typeable)

instance Exception CabalError 

------------------------------------------------------------------------
data ScionError = ScionError String
  deriving (Eq, Ord, Show, Generic, Typeable)

instance Exception ScionError

------------------------------------------------------------------------
data CompilationError =
  CompilationError FilePath [SourceError] [Warning]
  deriving (Eq, Ord, Show, Generic, Typeable)

instance Exception CompilationError

------------------------------------------------------------------------
data SourceError
  -- TODO: Add constructors for common error types (so an IDE can
  -- provide auto-fixing functionality).
  = OtherError !SourceSpan !T.Text
  deriving (Eq, Ord, Show, Generic)

instance Binary SourceError

instance ToJSON SourceError where
  toJSON (OtherError srcspan msg) =
    object [ "loc" .= toJSON srcspan,
             "severity" .= toJSON ("error" :: T.Text),
             "message" .= toJSON msg,
             "kind" .= object [ "type" .= toJSON ("other" :: T.Text) ] ]

------------------------------------------------------------------------
data Warning
  -- TODO: Add constructors for common warning types (so an IDE can
  -- provide auto-fixing functionality).
  = OtherWarning !SourceSpan !T.Text
  deriving (Eq, Ord, Show, Generic)

instance Binary Warning

instance ToJSON Warning where
  toJSON (OtherWarning span1 msg) =
    object [ "loc" .= toJSON span1,
             "severity" .= toJSON ("warning" :: T.Text),
             "message" .= toJSON msg,
             "kind" .= object [ "type" .= toJSON ("other" :: T.Text) ] ]

------------------------------------------------------------------------
data ModuleName = ModuleName !T.Text
  deriving (Eq, Ord, Show, Read, Generic)

mkModuleName :: T.Text -> ModuleName
mkModuleName = ModuleName

moduleName :: ModuleName -> T.Text
moduleName (ModuleName name) = name

instance Binary ModuleName where

------------------------------------------------------------------------
data ImportDependency = ImportDependency
  { importModuleName  :: !ModuleName
  , importPackageName :: !(Maybe T.Text)
  , importSource      :: !Bool
    -- ^ Whether the module is imported with @{-# SOURCE #-}@ pragma.
  , importSafe        :: !Bool
  } deriving (Eq, Ord, Show, Read, Generic)

instance Binary ImportDependency

------------------------------------------------------------------------
data ModuleHeader = ModuleHeader
  { moduleHeaderModuleName   :: !ModuleName
  , moduleHeaderOptions      :: [T.Text]
  , moduleHeaderImports      :: [ImportDependency]
  } deriving (Eq, Ord, Show, Read, Generic)

instance Binary ModuleHeader

------------------------------------------------------------------------
type InstalledPackage = InstalledPackageInfo_ ModuleName

deriving instance Eq m => Eq (InstalledPackageInfo_ m)

deriving instance Generic Version
deriving instance Generic License
deriving instance Generic PackageIdentifier
deriving instance Generic PackageName
deriving instance Generic InstalledPackageId
deriving instance Generic m => Generic (InstalledPackageInfo_ m)

instance Binary Version
instance Binary License
instance Binary PackageIdentifier
instance Binary PackageName
instance Binary InstalledPackageId
instance (Generic m, Binary m) => Binary (InstalledPackageInfo_ m)

{-
-- TODO: GHC 7.4.1 has a bug and cannot derive Generic for an external datatype with
-- type parameters.  As a workaround we serialise using Show/Read.
instance (Show m, Read m, Serialize m) =>
    Serialize (InstalledPackageInfo_ m) where
  put pkg_conf = put (show pkg_conf)
  get = read <$> get
-}
