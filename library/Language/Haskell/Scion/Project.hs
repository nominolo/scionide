{-# LANGUAGE DeriveDataTypeable, ExistentialQuantification,
    StandaloneDeriving, DeriveGeneric
 #-}
module Language.Haskell.Scion.Project 
  ( Project
  , mkCabalProject
  )
where

import Data.Binary
import GHC.Generics ( Generic )
import Control.Monad
import System.Directory
import System.FilePath

------------------------------------------------------------------------
data Project
  = CabalProject !FilePath
  deriving (Eq, Ord, Show, Read, Generic)

instance Binary ScionProject

------------------------------------------------------------------------
data ProjectConfig = ProjectConfig
  { projectDistDir :: FilePath
  } deriving (Eq, Ord, Show, Read, Generic)

instance Binary ProjectConfig

defaultConfig :: ProjectConfig
defaultConfig = ProjectConfig
  { projectDistDir = ".dist_scion"
  }

------------------------------------------------------------------------
mkCabalProject :: FilePath -> IO Project
mkCabalProject path0 = do
  path <- canonicalizePath path0
  when (takeExtension path /= "cabal") $
    fail "mkCabalProject: not a .cabal file"
  exists <- doesFileExist path
  unless exists $
    fail $ "mkCabalProject: File does not exist: " ++ path
  return $! CabalProject path

localModules :: ProjectConfig -> Project -> IO [ModuleName]
localModules conf (CabalProject cabalFile) = do
  return []