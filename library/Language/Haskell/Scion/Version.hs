module Language.Haskell.Scion.Version where

import Paths_scionide ( version )
import Data.Version

scionVersion :: [Int]
scionVersion = versionBranch version
