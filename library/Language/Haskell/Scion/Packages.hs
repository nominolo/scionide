module Language.Haskell.Scion.Packages
  ( InstalledPackage
  , packageName
  )
where

import Language.Haskell.Scion.Types

import qualified Distribution.Package as P
import Distribution.InstalledPackageInfo

packageName :: InstalledPackage -> String
packageName pkg =
  let P.PackageName name = P.packageName pkg in name
