{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE OverloadedStrings #-}
module Language.Haskell.Scion.Cabal where

import Language.Haskell.Scion.Types
import Language.Haskell.Scion.Utils.IO
import Language.Haskell.Scion.Dispatcher

import           Control.Applicative ( (<$>) )
import           Control.Concurrent.MVar
import           Control.Exception
import           Control.Monad ( filterM, when, forM )
import           Data.Binary ( encode, decodeFile, Binary )
import qualified Data.ByteString.Lazy as BL
import qualified Data.Map as M
import           Data.Maybe ( fromMaybe, isJust )
import qualified Data.Text as T
import           Data.Time.Clock
import           Development.Shake
import           Development.Shake.FilePath hiding ( (</>) )
import           System.Exit
import           System.FilePath
import           System.Directory hiding ( doesFileExist )
import           Distribution.Compiler ( CompilerId(..) )
import           Distribution.Package ( pkgName )
import qualified Distribution.PackageDescription.Parse as PD
import qualified Distribution.PackageDescription as PD
import           Distribution.Simple.Compiler ( Compiler(..) )
import           Distribution.Simple.Configure
import           Distribution.Simple.Build ( initialBuildSteps )
import qualified Distribution.Simple.LocalBuildInfo as Lbi
import qualified Distribution.ModuleName as Mdl
import           Distribution.Simple.GHC ( componentGhcOptions )
import           Distribution.Simple.Program
import           Distribution.Simple.Program.GHC ( GhcOptions(..),
                   renderGhcOptions, GhcMode(..), GhcOptimisation(..) )
import           Distribution.Simple.Setup ( defaultConfigFlags,
                     ConfigFlags(..), Flag(..) )
import           Distribution.Text ( display )
import qualified Distribution.Verbosity as V
import           GHC.Generics ( Generic )
-- import           GHC.Paths ( ghc, ghc_pkg )

import Debug.Trace

------------------------------------------------------------------------
data CabalConfig = CabalConfig
  { confDistDir   :: FilePath  -- Must be absolute path
  , confCabalFile :: FilePath  -- Must be absolute path
  , confRootDir   :: FilePath  -- Must be absolute path
  } deriving (Eq, Ord, Show)

-- | Create a new 'CabalConfig' from the path of a @.cabal@ file and
-- scratch directory.  If the scratch directory is not an absolute
-- path it will be considered relative to the directory containing the
-- @.cabal@ file.
mkCabalConfig :: FilePath -> FilePath -> IO CabalConfig
mkCabalConfig cabalFile0 distDir0 = do
  cabalFile <- liftIO $ canonicalizePath cabalFile0
  let rootDir = takeDirectory cabalFile
  let distDir = rootDir </> distDir0
  return $! CabalConfig distDir cabalFile rootDir

getCabalComponents :: FilePath -> IO [CabalComponent]
getCabalComponents path = do
  gpd <- PD.readPackageDescription V.silent path
  return $
    [ CLib (T.pack (display (pkgName (PD.package
           (PD.packageDescription gpd)))))
    | Just _ <- [PD.condLibrary gpd] ] ++
    [ CExe (T.pack name) | (name, _) <- PD.condExecutables gpd ]
    -- TODO: add test suites and benchmarks
 `catches` 
    [Handler $ \(e :: ExitCode) ->
      throwIO $ CabalError $ "Could not parse .cabal file: " ++
        show path
    , Handler $ \(e :: IOException) ->
      throwIO $ CabalError $ "Could not parse .cabal file: " ++
        show path ++ "\nReason: " ++ show e
    ]

------------------------------------------------------------------------

data CompilationMonitor = CompilationMonitor
  { cmNewMessages :: Messages }

mkCompilationMonitor :: CompilationMonitor
mkCompilationMonitor = CompilationMonitor M.empty

type Monitor = MVar CompilationMonitor

addFileMetadata :: Monitor -> FilePath
                -> FileMetadata -> [Warning] -> [SourceError]
                -> IO ()
addFileMetadata monit sourceFile meta warns errs = do
  modifyMVar_ monit $ \mon ->
    return mon{ cmNewMessages =
                   M.insert sourceFile (meta, warns, errs)
                            (cmNewMessages mon) }

------------------------------------------------------------------------

type CompileRuleImpl =
          FilePath   -- output file name
       -> FilePath   -- relative path inside output dir
       -> PD.BuildInfo
       -> FilePath   -- absolute path of output dir
       -> Lbi.LocalBuildInfo
       -> Lbi.ComponentLocalBuildInfo
       -> Action ()

------------------------------------------------------------------------
cabalRules :: CabalConfig -> Monitor -> MVar Workers -> Rules ()
cabalRules cfg monit workers = do

  -- A few utilities for computing paths inside the dist and root
  -- directories.
  let dist path = confDistDir cfg ++ path
  let distC comp path = (confDistDir cfg </> comp) ++ path
  let undist path = makeRelative (confDistDir cfg) path
  let undistC comp path = makeRelative (confDistDir cfg </> comp) path
  let root ('.':'/':path) = confRootDir cfg </> path
      root "." = confRootDir cfg
      root path = confRootDir cfg </> path
  let unroot path = makeRelative (confRootDir cfg) path

  dist "/setup-config" *> \_out -> do
    _lbi <- configureCabalProject (confCabalFile cfg) (dist "") monit
    return ()

  getLbi' <- newCache $ \file -> do
    getPersistBuildConfig (confDistDir cfg)

  let getLbi :: Action Lbi.LocalBuildInfo
      getLbi = getLbi' (dist "/setup-config")

  dist "//*.hi" *> \out -> do
    need [replaceExtension out "o"]

  -- TODO: We should generate full .meta files which include errors, and type-checked AST
  dist "//*.msgs" *> \out -> do
    need [replaceExtension out "hi"]

  let combineMsgFiles out msgFiles = do
        msgs <- forM msgFiles $ \msg -> do
                  (file :: FilePath, (header, warns, errs)) <- decodeFile' msg
                  return (file, (header, warns, errs :: [SourceError]))
        writeFileBL out (encode (M.fromList msgs :: Messages))

  distC "lib" "/.allmsgs" *> \out -> do
    lbi <- getLbi
    Just lib <- return $ PD.library (Lbi.localPkgDescr lbi)
    -- TODO: Use transitive dependencies
    let exposed = [ distC "lib" ("/" ++ Mdl.toFilePath m ++ ".msgs")
                  | m <- PD.libModules lib]
    need exposed
    combineMsgFiles out exposed

  -- A rule that relates to a file in a library component.
  let libRule :: String -> CompileRuleImpl -> Rules ()
      libRule pattern kont =
        distC "lib" pattern *> \out -> do
          lbi <- getLbi
          Just lib <- return $ PD.library (Lbi.localPkgDescr lbi)
          let bi = PD.libBuildInfo lib
          let relPath = dropExtension (undistC "lib" out)
          let Just clbi = Lbi.libraryConfig lbi
          kont out relPath bi (distC "lib/" "") lbi clbi

  -- A rule that relates to a file in an executable component
  let binRule :: String
              -> (String -> PD.Executable -> CompileRuleImpl) -> Rules ()
      binRule pattern kont =
        binRule' (\prefix file -> (prefix ++ pattern) ?== file) kont

      binRule' :: (FilePath -> FilePath -> Bool)
               -> (String -> PD.Executable -> CompileRuleImpl)
               -> Rules ()
      binRule' predicate kont = do
        predicate (distC "bin/*" "") ?> \out -> do
          lbi <- getLbi
          let binName:_ = splitDirectories (undistC "bin" out)
          let bins = PD.executables (Lbi.localPkgDescr lbi)
          case [ b | b <- bins, PD.exeName b == binName ] of
            [] -> error $ "Executable " ++ show binName ++ " does not exist in .cabal file"
            bin:_ -> do
              case [ clbi | (name, clbi) <- Lbi.executableConfigs lbi, name == binName ] of
                [] -> error $ "Executable " ++ show binName ++ " not configured"
                clbi:_ -> do
                  let bi = PD.buildInfo bin
                  let relPath = dropExtension (undistC ("bin/" ++ binName) out)
                  kont binName bin out relPath bi (distC ("bin/" ++ binName) "") lbi clbi

  let cabalRule :: String -> CompileRuleImpl -> Rules () 
      cabalRule pattern kont = do
        libRule pattern kont
        binRule pattern (\_ _ -> kont)

  let mkSearchPaths :: [FilePath] -> [FilePath]
      mkSearchPaths paths = dist "/build/autogen" : map root paths

  binRule "/.allmsgs" $ \binName bin out _relPath _bi _dist _lbi _clbi -> do
    let other = [ distC ("bin/" ++ binName) ("/" ++ Mdl.toFilePath m ++ ".msgs")
                | m <- PD.exeModules bin]
    let mainModule = distC ("bin/" ++ binName) "/Main.msgs"
    let binModules = mainModule : other
    -- TODO: May need to build and register local library first
    need binModules
    combineMsgFiles out binModules
    return ()

  -- The main module may not be called "Main.hs"
  binRule "/Main.dep" $ \_binName bin out _relPath bi _dist _lbi _clbi -> do
    let searchPaths = mkSearchPaths (PD.hsSourceDirs bi)
    Just hs <- findHsSource searchPaths (dropExtension (PD.modulePath bin))
    need [hs]
    (header, directImports) <- hsImports workers searchPaths hs
    writeFileBL out (encode (hs, header, directImports))

  binRule' (\prefix file -> 
               let a = (prefix ++ "//*.dep") ?== file
                   b = (prefix ++ "/Main.dep") ?== file
               in {-trace (show (prefix, file, a, b)) $ -} a && not b) $
   \_ _ out relPath bi _dist _lbi _clbi -> do
    let searchPaths = mkSearchPaths (PD.hsSourceDirs bi)
    let defaultFile = replaceExtension out "hs"
    hs <- fromMaybe defaultFile <$> findHsSource searchPaths relPath
    need [hs]
    (header, directImports) <- hsImports workers searchPaths hs
    writeFileBL out (encode (hs, header, directImports))

  libRule "//*.dep" $ \out relPath bi _dist _lbi _clbi -> do
    let searchPaths = mkSearchPaths (PD.hsSourceDirs bi)
    let defaultFile = replaceExtension out "hs"
    hs <- fromMaybe defaultFile <$> findHsSource searchPaths relPath
    need [hs]
    (header, directImports) <- hsImports workers searchPaths hs
    writeFileBL out (encode (hs, header, directImports))

  cabalRule "//*.o" $ \out relPath bi ldist lbi clbi -> do
    (hs, header, imports) <- decodeFile' (replaceExtension out "dep")
    let ifaces = [ ldist </> (moduleStringToFile "hi" imp)
                 | imp <- map (T.unpack . moduleName) imports ]
    need (hs : ifaces)
    compileFileCabal cfg monit workers Quick hs header out ldist lbi bi clbi

  cabalRule "//*.bco" $ \out relPath bi dist lbi clbi -> do
    (hs, header, imports) <- decodeFile' (replaceExtension out "dep")
    let ifaces = [ dist </> (moduleStringToFile "hi" imp)
                 | imp <- map (T.unpack . moduleName) imports ]
    need (hs : ifaces)
    compileFileCabal cfg monit workers TypecheckOnly hs header out dist lbi bi clbi


data CompileMode = TypecheckOnly | Quick | Optimizing
  deriving (Eq, Ord)

hsImports :: MVar Workers -> [FilePath] -> FilePath
          -> Action (ModuleHeader, [ModuleName])
hsImports workers searchPaths hsFile = do
  need [hsFile]
  header <- liftIO $ parseImports workers hsFile
  let imports = moduleHeaderImports header
  local_imports <- filterM isLocalImport imports
  return (header, map importModuleName local_imports)
 where
   isLocalImport imp = do
     -- Is this import from this package?
     let path = modulePath (importModuleName imp)
     isJust <$> findHsSource searchPaths path

-- | Search inside the search directories for the source file.  Tries
-- different extensions.
findHsSource :: [FilePath] -> FilePath -> Action (Maybe FilePath)
findHsSource searchPaths baseFile = do
  let extensions = ["hs"]  -- TODO: Import parsing currently doesn't
                           -- work with .lhs files.
  let candidates = [ path </> baseFile <.> ext
                   | path <- searchPaths
                   , ext <- extensions ]
  let search [] = return Nothing
      search (f:fs) = do
        ex <- doesFileExist f
        if ex then return (Just f) else search fs
  search candidates

decodeFile' :: Binary a => FilePath -> Action a
decodeFile' file = need [file] >> liftIO (decodeFile file)

------------------------------------------------------------------------
compileFileCabal :: CabalConfig -> Monitor -> MVar Workers
                 -> CompileMode
                 -> FilePath -> ModuleHeader
                 -> FilePath -> FilePath
                 -> Lbi.LocalBuildInfo -> PD.BuildInfo 
                 -> Lbi.ComponentLocalBuildInfo
                 -> Action ()
compileFileCabal _cfg monit workers mode sourceFile header outputFile
                 odir lbi bi clbi = do
  let tcOnly = mode == TypecheckOnly
      optimize = mode == Optimizing
  let opts0 = componentGhcOptions V.normal lbi bi clbi odir
      opts = opts0{ ghcOptMode = Flag GhcModeCompile
                  , ghcOptOptimisation = Flag (if optimize then
                                                 GhcNormalOptimisation
                                               else
                                                 GhcNoOptimisation)
                  }
      Compiler{compilerId=CompilerId _ version} = Lbi.compiler lbi
  let flags0 = "-fforce-recomp" : 
              [ f | f <- renderGhcOptions version opts, f /= "-c" ]
      flags | tcOnly    = "-fbyte-code" : flags0
            | otherwise = flags0

  traced ("=>> Compiling " ++ show sourceFile -- ++ " " ++ show flags
          ++ " => ") $ do
    timed (\t -> putStrLn $ "Compilation time: " ++ show t) $ do

      (warns, errs) <- compileFile workers sourceFile flags

      addFileMetadata monit sourceFile (HsFileMeta header) warns errs

      let enc = encode (sourceFile, (header, warns, errs))
      let msgFile = replaceExtension outputFile "msgs"
      BL.writeFile msgFile enc
      -- GHC won't generate any output, but we need to communicate to
      -- shake that an output was produced.
      when tcOnly $
        BL.writeFile outputFile ""
    
      when (not (null errs)) $ do
        liftIO $ throwIO $ CompilationError sourceFile errs warns

type Messages = M.Map FilePath (FileMetadata, [Warning], [SourceError])

data FileMetadata
  = HsFileMeta !ModuleHeader
  | CabalMeta -- TODO: Add stuff like configured dependencies?
  deriving (Eq, Ord, Show, Generic)

fileWarnings :: Messages -> FilePath -> Maybe [Warning]
fileWarnings msgs path =
  (\ (_, warns, _) -> warns) <$> M.lookup path msgs

instance Binary FileMetadata

timed :: (NominalDiffTime -> IO ()) -> IO a -> IO a
timed report act = do
  startTime <- getCurrentTime
  r <- act
  time <- (`diffUTCTime` startTime) <$> getCurrentTime
  report time
  return r

componentPrefix :: CabalComponent -> FilePath
componentPrefix comp = case comp of
  CLib _ -> "/lib"
  CExe name -> "/bin/" ++ safePath name
  CTest name -> "/test/" ++ safePath name
  CBench name -> "/bench/" ++ safePath name

componentMessagesPath :: CabalComponent -> FilePath
componentMessagesPath comp = componentPrefix comp ++ "/.allmsgs"

safePath :: T.Text -> FilePath
safePath s = T.unpack s   -- FIXME: Escape illegal characters

getComponentNotes :: CabalConfig -> MVar Workers -> CabalComponent
                  -> IO Messages
getComponentNotes cfg workers comp = do
  let allMsgs = componentMessagesPath comp
  monit <- newMVar mkCompilationMonitor
  withCurrentDirectory (confRootDir cfg) $ do
  timed (\t -> putStrLn $ "Shake time: " ++ show t) $ do
    (shake shakeOptions{ shakeVerbosity = Normal } $ do
      cabalRules cfg monit workers
      want [confDistDir cfg ++ allMsgs])
     `catch` (\(e :: ShakeException) ->
                 print e >> return ())
  msgs1 <- cmNewMessages <$> readMVar monit
  msgs2 <- decodeFile (confDistDir cfg ++ allMsgs)
             `catch` (\(_e :: IOException) -> return M.empty)
  let msgs =
        M.mapKeys (makeRelative (confRootDir cfg)) (msgs1 `M.union` msgs2)
  return msgs

getLibraryWarnings :: CabalConfig -> MVar Workers -> IO Messages
getLibraryWarnings cfg workers = getComponentNotes cfg workers (CLib "")

getExecutableWarnings :: CabalConfig -> MVar Workers -> T.Text
                      -> IO Messages
getExecutableWarnings cfg workers name =
  getComponentNotes cfg workers (CExe name)

getComponentNotesAfterChange :: CabalConfig -> MVar Workers -> CabalComponent
                             -> ModuleName -> IO Messages
getComponentNotesAfterChange cfg workers comp mdl = do
  let msgsFile = moduleStringToFile "bco" (T.unpack (moduleName mdl))
  let cprefix = componentPrefix comp
  monit <- newMVar mkCompilationMonitor
  withCurrentDirectory (confRootDir cfg) $ do
    timed (\t -> putStrLn $ "Shake time: " ++ show t) $ do
      (shake shakeOptions{ shakeVerbosity = Normal } $ do
         cabalRules cfg monit workers
         want [confDistDir cfg ++ cprefix ++ "/" ++ msgsFile])
       `catch` (\(e :: ShakeException) -> print e >> return ())
  msgs1 <- cmNewMessages <$> readMVar monit
  msgs2 <- decodeFile (confDistDir cfg ++ componentMessagesPath comp)
             `catch` (\(e :: IOException) -> return M.empty)
  let msgs =
        M.mapKeys (makeRelative (confRootDir cfg)) (msgs1 `M.union` msgs2)
  return msgs

getLibraryNotesAfterChange :: CabalConfig -> MVar Workers
                           -> ModuleName
                           -> IO Messages
getLibraryNotesAfterChange cfg workers mdl =
  getComponentNotesAfterChange cfg workers (CLib "") mdl

moduleStringToFile :: String -> String -> FilePath
moduleStringToFile ext mdl =
  map (\c -> if c == '.' then '/' else c) mdl <.> ext

configureCabalProject :: FilePath -> FilePath -> Monitor -> Action ()
configureCabalProject cabalFile dist monit = do
  startTime <- io getCurrentTime

  need [cabalFile]

  gen_pkg_descr <- io $ PD.readPackageDescription V.silent cabalFile

  -- TODO: The following only works for build-type simple.  We
  -- should support non-standard Setup.hs as well.

  -- TODO: Move into separate process, so we can use multiple versions
  -- of GHC.

  -- TODO: Make sure we configure with the same version of GHC
  let prog_conf =
        -- userSpecifyPaths [("ghc", ghc), ("ghc-pkg", ghc_pkg)]
          defaultProgramConfiguration

  let config_flags =
        (defaultConfigFlags prog_conf)
          { configDistPref = Flag dist
          , configVerbosity = Flag V.normal
          , configUserInstall = Flag True
          -- TODO: parse flags properly
          }

  io $ do
    lbi <- configure (gen_pkg_descr, (Nothing, [])) config_flags
    writePersistBuildConfig dist lbi
    initialBuildSteps dist (Lbi.localPkgDescr lbi) lbi V.normal

  io $ do
    time <- (`diffUTCTime` startTime) <$> getCurrentTime
    putStrLn $ "Cabal configure time: " ++ show time
    addFileMetadata monit cabalFile CabalMeta [] []
 where
   io act = liftIO $
     act `catches`
      [ Handler $ \(e :: ExitCode) -> do
          let msg = "Failed to configure: " ++ show e
          addFileMetadata monit cabalFile CabalMeta [] [cabalErrorMsg msg]
          throwIO $ CabalError msg
      , Handler $ \(e :: IOException) -> do
          let msg = "Failed to configure: " ++ show e
          addFileMetadata monit cabalFile CabalMeta [] [cabalErrorMsg msg]
          throwIO $ CabalError msg
      ]
   cabalErrorMsg msg =
     OtherError (SourceSpan 0 0 1 0)  -- whole first line
                (T.pack msg)

modulePath :: ModuleName -> FilePath
modulePath = T.unpack . T.replace "." "/" . moduleName

readFileBL :: FilePath -> Action BL.ByteString
readFileBL path = do
  need [path]
  liftIO $ BL.readFile path

writeFileBL :: FilePath -> BL.ByteString -> Action ()
writeFileBL path contents = liftIO $ BL.writeFile path contents
