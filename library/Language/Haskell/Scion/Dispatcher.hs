{-# LANGUAGE DeriveDataTypeable #-}
module Language.Haskell.Scion.Dispatcher where

import Language.Haskell.Scion.Version
import Language.Haskell.Scion.Types
import Language.Haskell.Scion.Utils.IO

import Data.Ord ( comparing )
import Data.List ( sortBy )
import System.Process
import System.Directory
import Control.Applicative
import Control.Monad ( when, forM_ )
import Control.Concurrent ( forkIO )
import Control.Concurrent.MVar
import Control.Exception ( Exception(..), throwIO, finally )
import Data.Typeable ( Typeable )
import Data.Time.Clock
import System.IO ( Handle, hIsEOF )
import qualified Data.Text as T
import qualified Data.ByteString.Char8 as B
import qualified Data.Map as Map

data WorkerHandle = WorkerHandle
  { workerProcess :: ProcessHandle
  , workerStdin   :: Handle
  , workerStdout  :: Handle
  , workerStderr  :: Handle
  }

defaultWorkerName :: String
defaultWorkerName = "scion-worker"

workerEnvVar :: String
workerEnvVar = "SCION_WORKER"

data WorkerNotFoundException = WorkerNotFoundException
  deriving (Eq, Typeable)

instance Show WorkerNotFoundException where
  show WorkerNotFoundException = unlines $
    [ "Could not find Scion worker executable.\n"
    , "Please make sure \"" ++ defaultWorkerName ++
      "\" is in the PATH or that"
    , "the environment variable " ++ workerEnvVar ++
      " points to an existing executable." ]

instance Exception WorkerNotFoundException where
  fromException = scionExceptionFromException
  toException = scionExceptionToException

data CannotStartWorker = CannotStartWorker String
  deriving (Eq, Typeable)

instance Show CannotStartWorker where
  show (CannotStartWorker msg) = "Cannot start worker: " ++ msg

instance Exception CannotStartWorker where
  fromException = scionExceptionFromException
  toException = scionExceptionToException

getWorkerExecutable :: IO FilePath
getWorkerExecutable = do
  mb_exe <- findExecutable =<< getWorkerName
  case mb_exe of
    Just exe -> return exe
    Nothing -> throwIO $ WorkerNotFoundException
 where
  getWorkerName = do
    mb_envname <- lookupEnv workerEnvVar
    case mb_envname of
      Just workerName -> return workerName
      Nothing -> return defaultWorkerName

expectedWorkerVersion :: [Int]
expectedWorkerVersion = scionVersion

startWorker :: IO WorkerHandle
startWorker = do
  worker <- getWorkerExecutable
  let workingDir = Nothing
      workerEnv = Nothing
  (inp, out, err, pid) <- runInteractiveProcess worker [] workingDir
                              workerEnv
  sendMessageToHandle inp GetWorkerVersion
  mb_ans <- recvMessageFromHandle out
  case mb_ans of
    Nothing -> do
      cleanup pid
      throwIO (CannotStartWorker "Did not respond")
    Just (WorkerVersion version)
      | version /= expectedWorkerVersion -> do
        cleanup pid
        throwIO $ CannotStartWorker $ 
          "Version mismatch. Expected " ++ show expectedWorkerVersion ++
          ", but got: " ++ show version
      | otherwise -> do
          _ <- forkIO $ readWorkerOutput err
          return ()
    _ -> do
      cleanup pid
      throwIO (CannotStartWorker "Unknown protocol")
  return WorkerHandle{ workerProcess = pid
                     , workerStdin = inp
                     , workerStdout = out
                     , workerStderr = err }
 where
   cleanup pid = do
     terminateProcess pid
     _ <- forkIO $ do
       -- TODO: Add a timeout
       _code <- getProcessExitCode pid
       return ()
     return ()
   readWorkerOutput err = loop
    where loop = do
            eof <- hIsEOF err
            if eof then return () else do
              line <- B.hGetLine err
              putStr ("worker[" ++ show (B.length line) ++ "]:")
              B.putStrLn line
              loop

-- | Sends a synchronous server command; i.e., will block until a
-- response is received.
workerCommand :: WorkerHandle -> WorkerCommand
              -> IO (Maybe WorkerResponse)
workerCommand handle cmd = do
  sendMessageToHandle (workerStdin handle) cmd
  recvMessageFromHandle (workerStdout handle)

stopWorker :: WorkerHandle -> IO ()
stopWorker handle = do
  ack <- workerCommand handle StopWorker
  when (ack /= Just WorkerAck) $
    terminateProcess (workerProcess handle)
  _ <- forkIO $ do
    -- TODO: Add a timeout
    _code <- waitForProcess (workerProcess handle)
    return ()
  return ()

-- | Initialise the GHC API for the worker.  Only call this function
-- once per worker.
setupWorkerGhcApi :: WorkerHandle
                  -> [String] -- ^ The command line flags.
                  -> IO [T.Text] -- ^ Warnings from parsing the flags.
setupWorkerGhcApi handle args = do
  ack <- workerCommand handle (InitGhcWorker (map T.pack args))
  case ack of
    Nothing ->
      cleanup "Error when starting up GHC API."
    Just (WorkerFailure msg) ->
      cleanup (T.unpack msg)
    Just (GhcWorkerReady warns) ->
      return warns
 where
   cleanup msg = do
     stopWorker handle
     throwIO $ CannotStartWorker msg

withWorker :: [String] -> (WorkerHandle -> [T.Text] -> IO a) -> IO a
withWorker flags act = do
  h <- startWorker
  warns <- setupWorkerGhcApi h flags
  act h warns `finally` stopWorker h

------------------------------------------------------------------------

data Workers = Workers
  { importParser :: !WorkerHandle
  , compilers :: Map.Map [String] (UTCTime, MVar WorkerHandle)
  }

initWorkers :: IO Workers
initWorkers = do
  import_parser <- startWorker
  _warns <- setupWorkerGhcApi import_parser []
  return $! Workers import_parser Map.empty

newWorkers :: IO (MVar Workers)
newWorkers = newMVar =<< initWorkers

stopWorkers :: MVar Workers -> IO ()
stopWorkers workers = do
  ws <- takeMVar workers
  stopWorker (importParser ws)
  forM_ (Map.elems (compilers ws)) $ \(_, wh) -> do
    stopWorker =<< readMVar wh

parseImports :: MVar Workers -> FilePath -> IO ModuleHeader
parseImports workers hsFile = do
  startTime <- getCurrentTime
  worker <- importParser <$> readMVar workers
  ans <- workerCommand worker (ParseImports hsFile)
  case ans of
    Just (ParsedImports header) -> do
      time <- (`diffUTCTime` startTime) <$> getCurrentTime
      putStrLn $ "Import parsing time: " ++ show time
      return header
    Just (WorkerSourceErrors errs) ->
      throwIO $ CompilationError hsFile errs []
    _ -> do
      error $ "Internal error when parsing imports of: " ++ hsFile ++ "\n"
              ++ show ans

maxActiveWorkers :: Int
maxActiveWorkers = 5  -- TODO: Make configurable

type Flags = [String]

compileFile :: MVar Workers -> FilePath -> [String]
            -> IO ([Warning], [SourceError])
compileFile workers sourceFile flags = do
  worker <- createWorkerIfNeeded workers flags
  ans <- workerCommand worker (CompileFile sourceFile)
  (case ans of
    Just (CompilationResult warns errs) ->
      return (warns, errs)
    _ ->
      error $ "Compiler failed: " ++ show ans) `finally`
        -- shut down worker asynchronously
        (do _ <- forkIO (stopWorker worker); return ())

createWorkerIfNeeded :: MVar Workers -> Flags -> IO WorkerHandle
createWorkerIfNeeded workers flags = do
  -- We cannot reliably reuse workers because instances (and other
  -- things) are cached which will lead to a conflict if we try to
  -- load the same code again.  For now we simply pre-load the worker
  -- process (saves about 50ms).
  mvar <- prepareNewWorker flags
  time <- getCurrentTime
  m <- modifyMVar workers $ \ws ->
           case Map.lookup flags (compilers ws) of
             Nothing -> do
               mvar' <- prepareNewWorker flags
               let ws' = ws{ compilers = 
                               Map.insert flags (time, mvar') (compilers ws) }
               return (ws', mvar)
             Just (_, mvar') -> do
               let ws' = ws{ compilers = 
                               Map.insert flags (time, mvar) (compilers ws) }
               return (ws', mvar')
  _ <- forkIO $ pruneWorkersIfNeeded workers
  takeMVar m

prepareNewWorker :: Flags -> IO (MVar WorkerHandle)
prepareNewWorker flags = do
  hdl <- newEmptyMVar
  _ <- forkIO $ do
         w <- startWorker
         _warns <- setupWorkerGhcApi w flags
         putMVar hdl w
  return hdl

setWorkerPending :: MVar Workers -> Flags -> MVar WorkerHandle -> IO ()
setWorkerPending workers flags hdl = do
  time <- getCurrentTime
  modifyMVar_ workers $ \ws -> do
    return ws{ compilers = Map.insert flags (time, hdl) (compilers ws) }

pruneWorkersIfNeeded :: MVar Workers -> IO ()
pruneWorkersIfNeeded workers = do
  -- TODO: also prune workers that haven't been used for N minutes
  modifyMVar_ workers $ \ws -> do
    if Map.size (compilers ws) < maxActiveWorkers then
      return ws
     else do
       putStrLn "Pruning"
       -- Remove using LRU policy
       let lastUsed = sortBy (comparing fst) $
             [ (lastUsed', key) | (key, (lastUsed', _)) <- Map.toList (compilers ws) ]
       let removeKey = snd (head lastUsed)
       return $ ws{ compilers = Map.delete removeKey (compilers ws) }
