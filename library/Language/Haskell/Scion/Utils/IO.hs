{-# LANGUAGE ScopedTypeVariables #-}
module Language.Haskell.Scion.Utils.IO where

import Control.Applicative
import Control.Exception ( handle, try, IOException, catchJust, finally )
import Control.Monad ( when )
import Data.Maybe ( isJust )
import Data.Binary
import Data.Binary.Get
import GHC.IO.Handle ( hDuplicateTo, hDuplicate )
import Network.Socket hiding (send, sendTo, recv, recvFrom)
import Network.Socket.ByteString hiding ( sendAll )
import Network.Socket.ByteString.Lazy ( sendAll )
import System.Directory ( getCurrentDirectory, setCurrentDirectory )
import System.Environment ( getEnv )
import System.IO
import System.IO.Error ( isDoesNotExistError )
import qualified Data.ByteString as S
import qualified Data.ByteString.Lazy as L

-- | Read a message from the Handle 
recvMessageFromHandle :: Binary a => Handle -> IO (Maybe a)
recvMessageFromHandle inp =
  recvMessage (hRecv inp chunkSize)

-- | Read a message from the socket.
recvMessageFromSocket :: Binary a => Socket -> IO (Maybe a)
recvMessageFromSocket sock =
  recvMessage (recv sock chunkSize)

-- | Receive a message from the given handle, blocks if no input is
-- available.
--
-- Returns an empty string if an error occurred (e.g., pipe
-- disconnected).
hRecv :: Handle
      -> Int -- ^ Maximum size of returned bytestring.
      -> IO S.ByteString
hRecv h size = do
  -- handle (\(e :: IOError) -> do
  --           putStrLn $ "IO Error: " ++ show e
  --           return S.empty) $ do
    -- Note: hWaitForInput tries to decode its input, so we must make
    -- sure the handle is in binary mode.
    eof <- hIsEOF h
    if eof then return S.empty else do
      hWaitForInput h (-1)
      S.hGetNonBlocking h size

-- | Reads and decodes a message using the given function to retrieve
-- a chunk.
recvMessage :: Binary a => IO S.ByteString
            -> IO (Maybe a)
recvMessage getChunk = do
  input <- getChunk
  go (runGetIncremental get `pushChunk` input)
 where
   go (Done _unusedInput _nConsumed result) =
     return (Just result)
   go (Fail _unused _nConsumed message) = do
     --putStrLn $ "recvMessage failed: " ++ message
     return Nothing
   go (Partial kont) = do
     input' <- getChunk
     if S.length input' > 0 then
       go (kont (Just input'))
      else
       go (kont Nothing)

-- | Send a message to a handle.
sendMessageToHandle :: Binary a => Handle -> a -> IO ()
sendMessageToHandle out message = do
  sendMessage (L.hPut out) message
  handle (\(e :: IOException) -> return ()) $ hFlush out

-- | Send a message to a socket.
sendMessageToSocket :: Binary a => Socket -> a -> IO ()
sendMessageToSocket sock message =
  sendMessage (sendAll sock) message

-- | Sends a message using the given function.  The message is sent
-- using a format readable by 'recvMessage'.
sendMessage :: Binary a => (L.ByteString -> IO ()) -> a -> IO ()
sendMessage send_str message_ = do
  send_str $! encode message_

-- | Maximum number of bytes to read from the input source.
chunkSize :: Int
chunkSize = 8000  -- use less than 8K to leave room for meta data

-- | Ensure that the handle is in binary mode.
ensureBinaryMode :: Handle -> IO ()
ensureBinaryMode h = do
  enc <- hGetEncoding h
  when (isJust enc) $
    hSetBinaryMode h True

-- | Get exclusive access to the first handle's resource.
--
-- Subsequent writes to the first handle are redirected to the second
-- handle.  The returned handle is an exclusive handle to the resource
-- initially held by the first handle.
makeExclusive ::
     Handle -- ^ The handle to the resource that we want exclusive
            -- access to.
  -> Handle -- ^ Anything written to the original handle will be
            -- redirected to this one.
  -> IO Handle -- ^ The exclusive handle.
makeExclusive hexcl hredirect = do
  hFlush hexcl
  hFlush hredirect
  hresult <- hDuplicate hexcl
  hDuplicateTo hredirect hexcl
  return hresult

-- | Look up the value of an environment variable.  Returns @Nothing@
-- if the variable is not defined.  E.g.,
--
-- > lookupEnv "PATH"   -- Just "/usr/bin"  or similar
-- > lookupEnv "not defined"  -- Nothing
--
lookupEnv :: String -> IO (Maybe String)
lookupEnv var = do
  (Just <$> getEnv var) `catchDoesNotExist` \_ -> return Nothing

-- | Variant of `catch` that only catches `isDoesNotExistError`
-- exceptions.
catchDoesNotExist :: IO a -> (IOException -> IO a) -> IO a
catchDoesNotExist body handler =
  catchJust (\ioerr ->
               if isDoesNotExistError ioerr then Just ioerr else Nothing)
    body handler

------------------------------------------------------------------------
-- | Execute the action in the given current directory.  Resets the
-- current directory after the action has completed or thrown an
-- exception.
--
-- Note that this operation is not thread safe.  Since the current
-- directory is a global property of the process it is shared by all
-- threads.
withCurrentDirectory :: FilePath -> IO a -> IO a
withCurrentDirectory dir act = do
  pwd <- getCurrentDirectory
  setCurrentDirectory dir
  act `finally` setCurrentDirectory pwd
