module Language.Haskell.Scion.Session where

import Language.Haskell.Scion.Types
import Language.Haskell.Scion.Cabal
import Language.Haskell.Scion.Dispatcher

--import Control.Applicative
import Control.Concurrent.MVar
import Data.IORef
import System.FilePath
import qualified Data.Map as M

data Session
  = CabalSession !CabalComponent !CabalConfig
                 !(IORef (M.Map FilePath FileMetadata))
  deriving (Eq)

-- instance Ord Session where
--   CabalSession comp cfg _ `compare` CabalSession comp' cfg' _ =
--     (cfg, comp) `compare` (cfg', comp')

makeCabalSession :: CabalComponent -> FilePath -> IO Session
makeCabalSession comp cabalFile = do
  cfg <- mkCabalConfig cabalFile ".scion"
  ref <- newIORef M.empty
  return $ CabalSession comp cfg ref

sessionRootDir :: Session -> FilePath
sessionRootDir (CabalSession _ cfg _) = confRootDir cfg

sessionNotes :: Session -> MVar Workers -> IO Messages
sessionNotes (CabalSession comp cfg ref) workers = do
  msgs <- getComponentNotes cfg workers comp
  atomicModifyIORef ref $ \_ ->
    (M.map (\(hdr, _, _) -> hdr) msgs, ())
  return msgs

sessionFiles :: Session -> IO (M.Map FilePath FileMetadata)
sessionFiles (CabalSession _ _ ref) =
  readIORef ref

sessionNotesAfterChange :: Session -> MVar Workers -> FilePath
                        -> IO (Maybe Messages)
sessionNotesAfterChange (CabalSession comp cfg ref) workers file = do
  files <- readIORef ref
  let relfile = makeRelative (confRootDir cfg) file
  case M.lookup relfile files of   
    Just (HsFileMeta header) -> do
      let mdl = moduleHeaderModuleName header
      msgs <- getComponentNotesAfterChange cfg workers comp mdl
      let msgs' = M.filterWithKey (\k _ -> k == relfile) msgs
      return $ Just msgs'
    _ -> return Nothing
