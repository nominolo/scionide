{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE ScopedTypeVariables #-}
module Main where

import Control.Applicative
import Control.Concurrent ( threadDelay )
import Control.Exception
import Control.Concurrent.MVar
import Data.Foldable ( forM_ )
import qualified Data.Foldable as F
import Data.Binary
import Data.List ( intersect, sort )
import Data.Maybe ( isJust, fromMaybe )
import qualified Data.Map as M
import qualified Data.Text as T
import qualified Data.Text.IO as T
import Language.Haskell.Scion.Dispatcher
import Language.Haskell.Scion.Types
import Language.Haskell.Scion.Packages
import Language.Haskell.Scion.Cabal
import System.Directory ( doesFileExist, getCurrentDirectory, removeFile,
          removeDirectoryRecursive, copyFile, getDirectoryContents )
import System.FilePath ( (</>), takeDirectory, takeFileName )
import System.IO.Temp ( withTempDirectory, withSystemTempDirectory )
import Test.Framework
import Test.Framework.Providers.HUnit
import Test.HUnit

------------------------------------------------------------------------

main = defaultMain tests

tests =
  [ testGroup "Serialisation"
    [ testCase "WorkerCommand" test_serializeWorkerCommand
    , testCase "WorkerResponse" test_serializeWorkerResponse
    ]
  , testGroup "Worker"
    [ testCase "start/stop" test_workerStartStop
    , testCase "Init GHC API" test_workerGhc
    , testCase "Init GHC API w/ warnings 1" test_workerGhc_withWarnings
    , testCase "Init GHC API w/ warnings 2" test_workerGhc_withWarnings2
    ]
  , testGroup "Commands"
    [ testGroup "Parse file options"
      [ testCase "succeed" test_workerGhc_parseFileOptions
      , testCase "unknown option" test_workerGhc_parseFileOptions2
      , testCase "parse error" test_workerGhc_parseFileOptions3
      , testCase "parse error 2" test_workerGhc_parseFileOptions4
      , testCase "unknown flag" test_workerGhc_parseFileOptions5
      , testCase "file does not exist" test_workerGhc_parseFileOptions6
      ]
    , testGroup "Parse imports"
      [ testCase "succeed" test_workerGhc_parseImports
      , testCase "parse error" test_workerGhc_parseImports2
      ]
    , testGroup "Parse Cabal"
      [ testCase "components" test_getCabalComponents
      , testCase "parse error" test_getCabalComponents2
      ]
    , testGroup "Compile"
      [ testCase "no messages" test_compile_simple1
      , testCase "1 warning" test_compile_simple2
      , testCase "header error" test_compile_error1
      , testCase "type error" test_compile_error2
      ]
    ]
  , testGroup "Project"
    [ testGroup "Shake"
      [ testCase "simple" test_shake_project1
      , testCase ".cabal error" test_shake_project1_broken
      , testCase "recompile" test_shake_project1_recompile
      ]
    ]
  ]

------------------------------------------------------------------------

testSerialization :: (Eq a, Show a, Binary a) => a -> IO ()
testSerialization thing =
  let bs = encode thing in
  case decodeOrFail bs of
    Left (_, _, err) ->
      assertFailure $ "Could not decode " ++ show thing
    Right (_, _, thing') ->
      thing @=? thing'

test_serializeWorkerCommand = do
  mapM_ testSerialization [ GetWorkerVersion
                          , CompileFile "foo.hs"
                          , StopWorker
                          ]

test_serializeWorkerResponse = do
  mapM_ testSerialization [ WorkerVersion 123
                          , UnknownCommand
                          , WorkerAck
                          , CompilationResult [] [] ]

------------------------------------------------------------------------

test_workerStartStop = do
  h <- startWorker
  stopWorker h

test_workerGhc = withWorker [] $ \h [] -> do
  ans <- workerCommand h GetInstalledPackages
  let Just (InstalledPackages pkgs) = ans
  let pkgNames = map packageName pkgs
  let expectedPackages = ["base", "ghc", "ghc-prim"]
  --mapM_ (print . installedPackageId) pkgs
  assertBool ("Packages should include " ++ show expectedPackages ++
             "\nGot: " ++ show pkgNames) $
    (expectedPackages `intersect` pkgNames) == expectedPackages

test_workerGhc_withWarnings = do
  h <- startWorker
  warns <- setupWorkerGhcApi h ["-smp"]
  stopWorker h
  ["-smp is deprecated: Use -threaded instead"] @=? warns

test_workerGhc_withWarnings2 = do
  h <- startWorker
  warns <- setupWorkerGhcApi h ["-foobarbaz"]
  stopWorker h
  ["Ignoring flags: [\"-foobarbaz\"]"] @=? warns

------------------------------------------------------------------------

test_workerGhc_parseFileOptions = withWorker [] $ \h [] -> do
  Just ans <- workerCommand h (ParseOptions "test/data/simple/test-opts.hs")
  case ans of
    ParsedOptions opts ->
      ["-Wall","-XBangPatterns","-XPatternGuards"] @=? sort opts
    _ -> assertFailure (show ans)

test_workerGhc_parseFileOptions2 = withWorker [] $ \h [] -> do
  Just ans <- workerCommand h (ParseOptions "test/data/simple/test-unknown-opt.hs")
  case ans of
    WorkerSourceErrors errs ->
      [OtherError (SourceSpan 1 14 1 23) "Unsupported extension: Kerfuffle"]
        @=? errs
    _ -> assertFailure (show ans)

test_workerGhc_parseFileOptions3 = withWorker [] $ \h [] -> do
  Just ans <- workerCommand h (ParseOptions "test/data/simple/test-opt-parse-error.hs")
  case ans of
    WorkerSourceErrors [OtherError _ text] ->
      "Cannot parse LANGUAGE pragma" @=? T.take 28 text
    _ -> assertFailure (show ans)

test_workerGhc_parseFileOptions4 = withWorker [] $ \h [] -> do
  Just ans <- workerCommand h (ParseOptions "test/data/simple/test-opt-parse-error2.hs")
  case ans of
    WorkerSourceErrors [OtherError _ text] ->
      "Cannot parse LANGUAGE pragma" @=? T.take 28 text
    _ -> assertFailure (show ans)

test_workerGhc_parseFileOptions5 = withWorker [] $ \h [] -> do
  Just ans <- workerCommand h (ParseOptions "test/data/simple/test-unknown-flag.hs")
  case ans of
    ParsedOptions opts ->
      -- Flags are verified later
      ["-auto-alibi"] @=? opts
    _ -> assertFailure (show ans)

test_workerGhc_parseFileOptions6 = withWorker [] $ \h [] -> do
  Just ans <- workerCommand h (ParseOptions "thisdoesnotexist/test5.hs")
  case ans of
    WorkerOtherError _ -> return ()
    _ -> assertFailure (show ans)

------------------------------------------------------------------------

test_workerGhc_parseImports = withWorker [] $ \h [] -> do
  Just ans <- workerCommand h (ParseImports "test/data/simple/test-opts.hs")
  case ans of
    ParsedImports header -> do
      mkModuleName "Main" @=? moduleHeaderModuleName header
      ["-Wall","-XBangPatterns","-XPatternGuards"] @=?
        sort (moduleHeaderOptions header)
      map mkModuleName ["Prelude"] @=?
        map importModuleName (moduleHeaderImports header)
    _ -> assertFailure (show ans)

test_workerGhc_parseImports2 = withWorker [] $ \h [] -> do
  Just ans <- workerCommand h (ParseImports "test/data/simple/test-opt-parse-error.hs")
  case ans of
    WorkerSourceErrors [OtherError _ text] ->
      "Cannot parse LANGUAGE pragma" @=? T.take 28 text
    _ -> assertFailure (show ans)

------------------------------------------------------------------------

test_getCabalComponents = do
  comps <- getCabalComponents "test/data/project1/project1.cabal"
  [CLib "project1",CExe "test1"] @=? comps

test_getCabalComponents2 = do
  comps <- try $ getCabalComponents "test/data/simple/cabal-parse-error.cabal"
  case comps of
    Left (e :: CabalError) -> return ()
    x -> assertFailure (show x)

------------------------------------------------------------------------

expectFileExists :: FilePath -> IO ()
expectFileExists file = do
  ex <- doesFileExist file
  assertBool ("File " ++ show file ++ " does not exist") ex

expectFileNotExists :: FilePath -> IO ()
expectFileNotExists file = do
  ex <- doesFileExist file
  assertBool ("File " ++ show file ++ " does exist") (not ex)

------------------------------------------------------------------------

test_compile_simple1 =
  withTempDirectory "test" "dist." $ \distDir -> do
  withWorker ["-odir", distDir, "-hidir", distDir] $ \h [] -> do
    Just ans <- workerCommand h (CompileFile "test/data/simple/test-opts.hs")
    case ans of
      CompilationResult [] [] -> do
        expectFileExists (distDir </> "Main.hi")
        expectFileExists (distDir </> "Main.o")
      _ -> assertFailure (show ans)

test_compile_simple2 =
  withTempDirectory "test" "dist." $ \distDir -> do
  withWorker ["-odir", distDir, "-hidir", distDir] $ \h [] -> do
    Just ans <- workerCommand h (CompileFile "test/data/simple/test-warn1.hs")
    case ans of
      CompilationResult [warn] [] -> do
        OtherWarning (SourceSpan 5 1 5 28)
          "Top-level binding with no type signature: main :: IO ()" @=? warn
        expectFileExists (distDir </> "Main.hi")
        expectFileExists (distDir </> "Main.o")
      _ -> assertFailure (show ans)

test_compile_error1 =
  withTempDirectory "test" "dist." $ \distDir -> do
  withWorker ["-odir", distDir, "-hidir", distDir] $ \h [] -> do
    Just ans <- workerCommand h (CompileFile "test/data/simple/test-opt-parse-error.hs")
    case ans of
      CompilationResult [] [_err] -> do
        expectFileNotExists (distDir </> "Main.hi")
        expectFileNotExists (distDir </> "Main.o")
      _ -> assertFailure (show ans)

test_compile_error2 =
  withTempDirectory "test" "dist." $ \distDir -> do
  withWorker ["-odir", distDir, "-hidir", distDir] $ \h [] -> do
    Just ans <- workerCommand h (CompileFile "test/data/simple/test-type-err1.hs")
    case ans of
      CompilationResult [] [OtherError _ msg] -> do
        "Couldn't match expected type `Int ->" @=? T.take 36 msg
        expectFileNotExists (distDir </> "Main.hi")
        expectFileNotExists (distDir </> "Main.o")
      _ -> assertFailure (show ans)

------------------------------------------------------------------------

sleep :: Int -> IO ()
sleep n = threadDelay (n * 1000000)

withProject :: FilePath -> (CabalConfig -> MVar Workers -> IO a) -> IO a
withProject cabalPath kont = do
  cfg <- mkCabalConfig cabalPath ".scion"
  workers <- newWorkers
  kont cfg workers `finally` do
    removeDirectoryRecursive (takeDirectory cabalPath </> ".scion")
    stopWorkers workers

isDotFile :: FilePath -> Bool
isDotFile ('.':_) = True
isDotFile _ = False

-- | Copies the directory containing the given Cabal file into a
-- temporary directory and invokes the callback.  The temporary
-- directory is removed automatically after the callback returns (or
-- throws an exception).
withCopiedProject :: FilePath -> (CabalConfig -> MVar Workers -> IO a) -> IO a
withCopiedProject cabalPath kont = do
  let srcDir = takeDirectory cabalPath
  withSystemTempDirectory "scion-test" $ \destDir -> do
    -- TODO: Copy recursively
    project_files <- filter (not . isDotFile) <$> getDirectoryContents srcDir
    forM_ project_files $ \file -> do
      --putStrLn $ "Copy " ++ (srcDir </> file) ++ " => " ++ (destDir </> file)
      copyFile (srcDir </> file) (destDir </> file)
    let cabalPath' = destDir </> takeFileName cabalPath
    cfg <- mkCabalConfig cabalPath' ".scion"
    workers <- newWorkers
    kont cfg workers `finally` do
      --removeDirectoryRecursive (takeDirectory cabalPath </> ".scion")
      stopWorkers workers

------------------------------------------------------------------------

test_shake_project1 =
  withProject "test/data/project1/project1.cabal" $ \cfg workers -> do
    msg1 <- getComponentNotes cfg workers (CLib "project1")
    assertEqual "Number of files in project" 3 (M.size msg1)

test_shake_project1_broken = do
  withCopiedProject "test/data/project1/project1.cabal" $ \cfg workers -> do
    -- Break the .cabal file
    cabalFile <- T.readFile (confCabalFile cfg)
    T.writeFile (confCabalFile cfg) $ T.replace "base" "base1" cabalFile

    msg1 <- getComponentNotes cfg workers (CLib "project1")
    assertEqual "Number of files with messages" 1 (M.size msg1)
    assertBool "The message is for project1.cabal" $
      isJust (M.lookup "project1.cabal" msg1)

-- | Fix warnings or errors by uncommenting special "-- FIX: " lines.
fixFile :: FilePath -> IO ()
fixFile path = T.readFile path >>= return . fixit >>= T.writeFile path
 where
   fixit = T.unlines . map fixOne  . T.lines
   fixOne line = fromMaybe line (T.stripPrefix "-- FIX: " line)

test_shake_project1_recompile = do
  withCopiedProject "test/data/project1/project1.cabal" $ \cfg workers -> do
    msgs1 <- getComponentNotes cfg workers (CLib "project1")
    let warnsB = F.concat $ fileWarnings msgs1 "TestB.hs"
    assertEqual "Number of warnings in TestB after initial compilation"
      2 (length warnsB)

    -- Immediately asking for errors again should give same answer
    msgs2 <- getComponentNotes cfg workers (CLib "project1")
    let warnsB = F.concat $ fileWarnings msgs2 "TestB.hs"
    assertEqual "Number of warnings in TestB after no changes"
      2 (length warnsB)

    sleep 1  -- Make sure the timestamp is different
    fixFile (confRootDir cfg </> "TestB.hs")
    msgs3 <- getComponentNotes cfg workers (CLib "project1")
    let warnsB = F.concat $ fileWarnings msgs3 "TestB.hs"
    assertEqual "Number of warnings in TestB after fixing"
      0 (length warnsB)
