{-# OPTIONS_GHC -Wall #-}
module TestB where

g :: Int -> Int
g n | n < 5     = 32
    | otherwise = n * 4

-- FIX: g1 :: Int
g1 = 23
