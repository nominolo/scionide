{-# OPTIONS_GHC -Wall #-}
module Main ( main ) where

-- main :: IO ()  -- cause a warning
main = putStrLn "Hello me!"

err = (1 :: Int) (2 :: Int)
